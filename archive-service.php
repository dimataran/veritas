<?php
global $custom_style;
get_header('law');
?>

    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php $custom_style ->theImgPath() ?>background/4.jpg)">
        <div class="container">
            <div class="content">
                <h1><?php post_type_archive_title(); ?></h1>
                <ul class="page-breadcrumb">
                    <li><a href="/"><?php _e('Головна', ''); ?></a></li>
                    <li><?php post_type_archive_title(); ?></li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Services Section Two -->
    <section class="services-section-four">
        <div class="container">
            <!-- Sec Title -->
            <div class="section-title centered">
                <div class="title">Послуги</div>
                <h3>Успішне завершення вашої справи — <br> наша <span>головна мета!</span></h3>
            </div>
	        
	        <?php  get_template_part('template-parts/global/services', null, ['additinal_css_class' => 'style-two']) ?>
         

        </div>
    </section>
    <!-- End Services Section Two -->

    <!-- Pricing Section -->
     <?php // get_template_part('template-parts/global/pricing') ?>


    <!-- End Pricing Section -->

<?php // get_template_part('template-parts/subscription') ?>


<?php get_footer('law');