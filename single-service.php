<?php
get_header('law');
global $themefunc;

   $services_cat_terms = $terms = get_terms( [
       'taxonomy' => 'service_cat',
       'hide_empty' => true,
   ] );

$post_id = get_the_ID();
$args = $themefunc->getQueryArgs( 'service' );
$services_query = new WP_Query($args);


$url = get_the_post_thumbnail_url( null, 'Large' );
    if (empty($url)) {
    $url = 'https://www.advokat-veritas.kh.ua/wp-content/uploads/2021/01/vzyatka-300x175.jpg';
    }
    ?>
<style>
    .blog-cat li a {
        padding-right: 18px;
    }
</style>
    <section class="page-title" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(<?php echo $url ?>)">
        <div class="container">
            <div class="content">
                <h1><?php the_title() ?></h1>
                <ul class="page-breadcrumb">
                    <li><a href="/"><?php _e('Головна', ''); ?></a></li>
                    <li><?php the_title() ?></li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
    <div class="container">
        <div class="row clearfix">

            <!--Sidebar Side-->
            <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
                <aside class="sidebar padding-right">

                    <!--Blog Category Widget-->
                    <div class="sidebar-widget sidebar-blog-category">
                        <?php

                        if( $services_query->have_posts() ): ?>
                        <ul class="blog-cat">
                            <?php while( $services_query->have_posts() ): $services_query->the_post();
                                $add_class = '';
                            setup_postdata($post);
                            if ( $post_id === $post->ID ) {
                                $add_class = ' class="active"';
                            }
                            ?>

                            <li<?php echo $add_class ?>><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></li>
                             <?php

                            endwhile;
                             wp_reset_postdata();
                             ?>

                        </ul>
                        <?php endif ?>
                    </div>

                    <!-- Contact Widget-->
                    <div class="sidebar-widget contact-widget">
                        <div class="sidebar-title">
                            <h4>Contact</h4>
                        </div>
                        <ul>
                            <li><span class="icon flaticon-map-1"></span>Проспект Героїв Харкова, 257, оф 721 <br> Харків
                              </li>
                            <li><span class="icon flaticon-call-answer"></span>  063-868-58-40</li>
<!--                            <li><span class="icon flaticon-comment"></span> support@lawsight.com</li>-->
                        </ul>
                    </div>

                    <!-- Brochures Widget-->
                    <!--
                    <div class="sidebar-widget brochures-widget">
                        <div class="sidebar-title">
                            <h4>Brochures</h4>
                        </div>
                        <div class="text">View our 2019 financial prospectus brochure for an easy to read guide on all of the services offer.</div>
                        <ul class="files">
                            <li><a href="#"><span class="flaticon-download-arrow"></span> Download Brochure</a></li>
                            <li><a href="#"><span class="flaticon-pdf"></span> Characteristics</a></li>
                        </ul>
                    </div>
                    -->


                </aside>
            </div>

            <!--Content Side-->
            <div class="content-side col-lg-8 col-md-12 col-sm-12">
                <div class="services-single">
                    <h4><?php the_title() ?></h4>
                    <div class="text">
                        <?php echo apply_filters('the_content', get_the_content()) ?>

                    </div>

                   
                    <!-- Services Gallery -->
                    <!--
				   <div class="services-gallery">
					   <div class="services-carousel owl-carousel owl-theme">
						   <div class="slide">
							   <div class="image">
								   <img src="images/resource/service-2.jpg" alt="">
							   </div>
						   </div>
						   <div class="slide">
							   <div class="image">
								   <img src="images/resource/service-3.jpg" alt="">
							   </div>
						   </div>
						   <div class="slide">
							   <div class="image">
								   <img src="images/resource/service-2.jpg" alt="">
							   </div>
						   </div>
						   <div class="slide">
							   <div class="image">
								   <img src="images/resource/service-3.jpg" alt="">
							   </div>
						   </div>
						   <div class="slide">
							   <div class="image">
								   <img src="images/resource/service-2.jpg" alt="">
							   </div>
						   </div>
						   <div class="slide">
							   <div class="image">
								   <img src="images/resource/service-3.jpg" alt="">
							   </div>
						   </div>
					   </div>
				   </div>
				   -->

				   <!--Services Info Tabs-->
                    <!--
                    <div class="Services-info-tabs">
                    
                        <!--Service Tabs-->
                    <!--
                        <div class="service-tabs tabs-box">
                            -->
                    
                            <!--Tab Btns-->
                    
<!--                            <ul class="tab-btns tab-buttons clearfix">-->
<!--                                <li data-tab="#prod-audit" class="tab-btn active-btn"><i>Audit</i></li>-->
<!--                                <li data-tab="#prod-strategy" class="tab-btn"><i>Strategy</i></li>-->
<!--                                <li data-tab="#prod-sustainability" class="tab-btn"><i>Sustainability</i></li>-->
<!--                            </ul>-->

                            <!--Tabs Container-->
<!--                            <div class="tabs-content">-->

                                <!--Tab / Active Tab-->
<!--                                <div class="tab active-tab" id="prod-audit">-->
<!--                                    <div class="content">-->
<!--                                        <div class="text">-->
<!--                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudan tium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>-->
<!--                                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequun tur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->

                                <!--Tab-->
<!--                                <div class="tab" id="prod-strategy">-->
<!--                                    <div class="content">-->
<!--                                        <div class="text">-->
<!--                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudan tium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>-->
<!--                                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequun tur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->

                                <!--Tab-->
                                
<!--                                <div class="tab" id="prod-sustainability">-->
<!--                                    <div class="content">-->
<!--                                        <div class="text">-->
<!--                                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudan tium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>-->
<!--                                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequun tur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->

<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
              

                <!--
                    <div class="row clearfix">
    -->
                        <!-- Services Block Two -->
<!--                        <div class="services-block-two style-two col-lg-6 col-md-6 col-sm-12">-->
<!--                            <div class="inner-box">-->
<!--                                <div class="icon-box">-->
<!--                                    <span class="icon flaticon-internet"></span>-->
<!--                                </div>-->
<!--                                <h3>Business Law</h3>-->
<!--                                <div class="text">It is a long established fact that areader will be distracted by the readable content of a page when looking.</div>-->
<!--                                <div class="overlay-box" style="background-image: url(images/resource/service-1.jpg);">-->
<!--                                    <div class="overlay-inner">-->
<!--                                        <div class="content">-->
<!--                                            <span class="icon flaticon-internet"></span>-->
<!--                                            <h4><a href="#">Business Law</a></h4>-->
<!--                                            <a href="#" class="theme-btn btn-style-one">consult now</a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->

                        <!-- Services Block Two -->
<!--                        <div class="services-block-two style-two col-lg-6 col-md-6 col-sm-12">-->
<!--                            <div class="inner-box">-->
<!--                                <div class="icon-box">-->
<!--                                    <span class="icon flaticon-museum"></span>-->
<!--                                </div>-->
<!--                                <h3>Civil Law</h3>-->
<!--                                <div class="text">It is a long established fact that areader will be distracted by the readable content of a page when looking.</div>-->
<!--                                <div class="overlay-box" style="background-image: url(images/resource/service-1.jpg);">-->
<!--                                    <div class="overlay-inner">-->
<!--                                        <div class="content">-->
<!--                                            <span class="icon flaticon-museum"></span>-->
<!--                                            <h4><a href="#">Civil Law</a></h4>-->
<!--                                            <a href="#" class="theme-btn btn-style-one">consult now</a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                    </div>-->
<!--                -->

                    <!-- More Services -->
                    <!--
                    <div class="more-services">
                        <div class="clearfix">
                            <?php
                            $prev_post = get_adjacent_post(false, '', true, 'service_cat');
                            if( $prev_post ): ?>
                            <div class="pull-left">
                                <a href="<?php echo get_permalink($prev_post->ID) ?>"><span class="fa fa-angle-double-left"></span> Previous Service</a>
                            </div>
                            <?php endif; ?>
                            <?php
                            $next_post = get_adjacent_post(false, '', false, 'service_cat');
                            if( $next_post ): ?>
                            <div class="pull-right">
                                <a href="<?php echo get_permalink($next_post->ID) ?>">Newer Service <span class="fa fa-angle-double-right"></span></a>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    -->

                </div>
            </div>

        </div>
    </div>
</div>
    <!--End Sidebar Page Container-->


    <!-- Subscribe Section -->

    <?php // get_template_part('template-parts/subscription') ?>

    <!-- End Subscribe Section -->

    <?php get_footer('law') ?>
