<?php
global $custom_style;
global $themefunc;
/*
 * Template name: Contact
*/

get_header('law'); ?>


    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php $custom_style->theImgPath() ?>background/4.jpg)">
        <div class="container">
            <div class="content">
                <h1><?php the_title(); ?></h1>
                <ul class="page-breadcrumb">
                    <li><a href="/"><?php _e('Головна', ''); ?></a></li>
                    <li><?php the_title(); ?></li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Contact Page Section -->
    <section class="contact-page-section">
        <div class="map-section">
            <!--Map Outer-->
            <div class="map-outer">
                <!--Map Canvas-->
                <div class="map-canvas"
                     data-zoom="12"
                     data-lat="-37.817085"
                     data-lng="144.955631"
                     data-type="roadmap"
                     data-hue="#ffc400"
                     data-title="Envato"
                     data-icon-path="<?php $custom_style->theImgPath() ?>icons/map-marker.png"
                     data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="inner-container">
                <h2> Звернення кваліфікованого адвоката <br>збереже ваш <span>час, гроші та спокій</span>!</h2>
                <div class="row clearfix">
                   
                    
                    
                    <!-- Info Column -->
                    <div class="info-column col-lg-7 col-md-12 col-sm-12">
                        <div class="inner-column">
                            <div class="text">Якщо у вас виникли проблеми з законом, будь-які адміністративні правові питання або просто потрібна консультація фахівця — чекаємо на вас!</div>
                            <ul class="list-style-six">
                                <li><span class="icon fa fa-building"></span> Проспект Героїв Харкова,<br> 257, оф 721, Україна</li>
                                <li><span class="icon fa fa-fax"></span> 063-868-58-40</li>
<!--                                <li><span class="icon fa fa-envelope-o"></span>support@lawsight.com</li>-->
                            </ul>
                        </div>
                    </div>

                    <!-- Form Column -->
                    <div class="form-column col-lg-5 col-md-12 col-sm-12">
                        <div class="inner-column">

                            <!--Contact Form-->

                            <div class="contact-form">
                                <?php
                                $short_code_form = '[contact-form-7 id="' . $themefunc->get_cf7_id('362', '362') . '"  html_id="contact-form" ]';
                                echo do_shortcode( $short_code_form ); ?>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End Team Page Section -->

    <!-- Contact Info Section -->
    <section class="contact-info-section" style="background-image:url(<?php $custom_style->theImgPath() ?>background/5.jpg)">
        <div class="container">
            <div class="row clearfix">

                <div class="column col-lg-4 col-md-6 col-sm-12">
                    <h4>United Kingdom</h4>
                    <ul class="list-style-seven">
                        <li><span class="icon flaticon-map-1"></span> 49488 Avenida Obregon, <br> La Quinta, CA 92253</li>
                        <li><span class="icon flaticon-call-answer"></span> +1-(281)-813 926 <br> +1-(281)-813 612</li>
                        <li><span class="icon fa fa-envelope-o"></span>support@lawsight.com.uk</li>
                    </ul>
                </div>
                <div class="column col-lg-4 col-md-6 col-sm-12">
                    <h4>Australia</h4>
                    <ul class="list-style-seven">
                        <li><span class="icon flaticon-map-1"></span> 13/1 Dixon Street, Sydney <br> NSW 2000</li>
                        <li><span class="icon flaticon-call-answer"></span> +1-(281)-813 926 <br> +1-(281)-813 612</li>
                        <li><span class="icon fa fa-envelope-o"></span>support@lawsight.com.uk</li>
                    </ul>
                </div>
                <div class="column col-lg-4 col-md-6 col-sm-12">
                    <h4>Netherlands</h4>
                    <ul class="list-style-seven">
                        <li><span class="icon flaticon-map-1"></span> Nieuwe Leliestraat 27-HS <br> 101J Amsterdam</li>
                        <li><span class="icon flaticon-call-answer"></span> +1-(281)-813 926 <br> +1-(281)-813 612</li>
                        <li><span class="icon fa fa-envelope-o"></span>support@lawsight.com.uk</li>
                    </ul>
                </div>

            </div>
        </div>
    </section>
    <!-- End Contact Info Section -->

<?php get_footer('law'); ?>