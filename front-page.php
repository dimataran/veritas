<?php
use  AstiDivi\Classes\Image;
global $custom_style;
global $themefunc;

/*
Template Name: Front Page
*/

get_header('law'); ?>


<!--Banner Section-->
<section class="banner-section">
    <div class="main-slider-carousel owl-carousel owl-theme">
        <!-- Slide -->
        <div class="slide" style="background-image:url(<?php $custom_style->theImgPath() ?>main-slider/1.jpg)">
            <div class="container">
                <div class="content">
                    <div class="title">Адвокат Г. Бережной</div>
                    <h1>Ми працюємо, щоб захистити <br> ваші <span>права</span></h1>
                    <a href="<?php echo get_home_url(null, '/') ?>/advokat-kharkov-contacts" class="theme-btn btn-style-one">Зв'язатись з нами</a>
                </div>
            </div>
        </div>

    </div>
</section>
<!--End Banner Section-->

<!-- Services Section -->
<section class="services-section">
    <div class="icon-one wow zoomIn" data-wow-delay="250ms" data-wow-duration="1500ms" style="background-image:url(<?php $custom_style->theImgPath() ?>icons/icon-1.png)"></div>
    <div class="icon-two wow fadeInRight" data-wow-delay="500ms" data-wow-duration="1500ms" style="background-image:url(<?php $custom_style->theImgPath() ?>icons/icon-2.png)"></div>
    <div class="container">
        <div class="row clearfix">

            <!-- Services Block -->
            <div class="services-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="border-one"></div>
                    <div class="border-two"></div>
                    <div class="content">
                        <div class="icon-box">
                            <span class="icon flaticon-family"></span>
                        </div>
                        <h6><a href="<?php echo get_home_url(null, '/') ?>/advokat-kharkov-service/advokat-kharkov-price-service.html">125+ виграних справ у суді</a></h6>
                        <div class="text">Убивства, крадіжки і пограбування, незаконний обіг наркотичних речовин, та інші кейси</div>
                    </div>
                </div>
            </div>

            <!-- Services Block -->
            <div class="services-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="border-one"></div>
                    <div class="border-two"></div>
                    <div class="content">
                        <div class="icon-box">
                            <span class="icon flaticon-fingerprint-1"></span>
                        </div>
                        <h6><a href="<?php echo get_home_url(null, '/') ?>/advokat-kharkov-service/price-advokat-kharkov-criminal.html">Кримінальні справи</a></h6>
                        <div class="text">Це наша основна спеціалізація. Розглядаємо будь-які, навіть найскладніші випадки</div>
                    </div>
                </div>
            </div>

            <!-- Services Block -->
            <div class="services-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="border-one"></div>
                    <div class="border-two"></div>
                    <div class="content">
                        <div class="icon-box">
                            <span class="icon flaticon-non-stop-call"></span>
                        </div>
                        <h6><a href="<?php echo get_home_url(null, '/') ?>/criminal-advokat-kharkov-service">Нам довіряють</a></h6>
                        <div class="text">Професійний та індивідуальний підхід до кожної справи — запорука нашого успіху</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- End Services Section -->

<!-- About Section -->
<section class="about-section">
    <!-- Image Layer -->
    <div class="image-layer" style="background-image:url(<?php $custom_style->theImgPath() ?>resource/about-1.jpg)"></div>
    <div class="container">
        <div class="row clearfix">

            <!-- Content Column -->
            <div class="content-column col-lg-7 col-md-12 col-sm-12">
                <div class="inner-column">

                    <!-- Sec Title -->
                    <div class="section-title">
                        <div class="title"> Про нас</div>
                        <h3>Ми завжди поруч та <span>готові вирішити</span> ваші проблеми</h3>
                    </div>
                    <div class="text">
                        <p><span>Адвокат</span> — це юридичний консультант та захисник як громадян, так і юридичних осіб. Він зобов'язаний розбиратися (і розбирається) у великому спектрі правової інформації.
                        <p>Як отримати допомогу адвоката?
                    </div>
                    <div class="row clearfix">
                        <div class="column col-lg-6 col-md-6 col-sm-12">
                            <ul class="list-style-one">
                                <li>Зв'яжіться з нами телефоном та розкажіть про свій випадок</li>
                            </ul>
                        </div>
                        <div class="column col-lg-6 col-md-6 col-sm-12">
                            <ul class="list-style-one">
                                <li>Отримайте відповідь про те, що дане питання вирішується в юридичній площині</li>
                            </ul>
                        </div>
                        <div class="column col-lg-6 col-md-6 col-sm-12">
                            <ul class="list-style-one">
                                <li>Прийдіть на консультацію та підпишіть договір (за необхідності)</li>
                            </ul>
                        </div>
                        <div class="column col-lg-6 col-md-6 col-sm-12">
                            <ul class="list-style-one">
                                <li>Отримайте детальне розв'язання вашого питання</li>
                            </ul>
                        </div>
                    </div>
                    <div class="question">Телефони <strong>063-868-58-40</strong></div>
                    <div class="signature">
                        <div class="signature-img"><img src="<?php $custom_style->theImgPath() ?>icons/signature.png" alt=""/></div>
                        <h5>Геннадій Бережний</h5>
                        <div class="designation">(Адвокат)</div>
                    </div>
                </div>
            </div>

            <!-- Image Column -->
            <div class="image-column col-lg-5 col-md-12 col-sm-12">
                <div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="image">
                        <img src="<?php $custom_style->theImgPath() ?>resource/about-2.jpg" alt=""/>
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="content">
                                    <h2><span>Працюємо з</span> 2011 <span>року</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
<!-- End About Section -->

<!-- Services Section Two -->
<section class="services-section-two" style="background-image: url(<?php $custom_style->theImgPath() ?>background/1.jpg);">
    <div class="container">
        <!-- Sec Title -->
        <div class="section-title light centered">
            <div class="title">Наші послуги</div>
            <h3>Успішне завершення вашої справи — <br> наша <span>головна мета!</span></h3>
        </div>
        <div class="row clearfix">
           
            <!-- Services Block Two -->
            <div class="services-block-two col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon flaticon-internet"></span>
                    </div>
                    <h3>Адвокат по наркотиках</h3>
                    <div class="text">Допомагаємо у вирішенні кримінальних проваджень за ст. 307 УК України</div>
                    <div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
                        <div class="overlay-inner">
                            <div class="content">
                                <span class="icon flaticon-internet"></span>
                                <h4><a href="<?php echo get_home_url(null, '/') ?>/advokat-kharkov-service/advokat-po-narkotikam-kharkov.html">Адвокат по наркотиках</a></h4>
                                <a href="<?php echo get_home_url(null, '/') ?>/advokat-kharkov-service/advokat-po-narkotikam-kharkov.html" class="theme-btn btn-style-one">Записатися</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Services Block Two -->
            <div class="services-block-two col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon flaticon-museum"></span>
                    </div>
                    <h3>Цивільне право</h3>
                    <div class="text">Захищаємо та представляємо вашу сторону у суперечках з приватними або юридичними особами та державою</div>
                    <div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
                        <div class="overlay-inner">
                            <div class="content">
                                <span class="icon flaticon-museum"></span>
                                <h4><a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/">Цивільне право</a></h4>
                                <a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/" class="theme-btn btn-style-one">Записатися</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Services Block Two -->
            <div class="services-block-two col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon flaticon-gun"></span>
                    </div>
                    <h3>Крадіжки й пограбування</h3>
                    <div class="text">Надаємо консультацію за всіма запитаннями, що стосуються справи. Захищаємо ваші інтереси в суді</div>
                    <div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
                        <div class="overlay-inner">
                            <div class="content">
                                <span class="icon flaticon-gun"></span>
                                <h4><a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/">Крадіжки й пограбування</a></h4>
                                <a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/" class="theme-btn btn-style-one">Записатися</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Services Block Two -->
            <div class="services-block-two col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon flaticon-plan"></span>
                    </div>
                    <h3>Особисті консультації</h3>
                    <div class="text">Ми — ваша надійна опора в розв'язанні будь-яких юридичних питань</div>
                    <div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
                        <div class="overlay-inner">
                            <div class="content">
                                <span class="icon flaticon-plan"></span>
                                <h4><a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/">Особисті консультації</a></h4>
                                <a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/" class="theme-btn btn-style-one">Записатися</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Services Block Two -->
            <div class="services-block-two col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon flaticon-book"></span>
                    </div>
                    <h3>Виконавчі провадження</h3>
                    <div class="text">Допомога в стягненні заборгованості</div>
                    <div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
                        <div class="overlay-inner">
                            <div class="content">
                                <span class="icon flaticon-book"></span>
                                <h4><a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/">Виконавчі провадження</a></h4>
                                <a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/" class="theme-btn btn-style-one">Записатися</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Services Block Two -->
            <div class="services-block-two col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon flaticon-house-outline"></span>
                    </div>
                    <h3>Захист ваших прав</h3>
                    <div class="text">Адвокат пильно вивчає всі матеріали справи та стежить, щоб ваші права не порушувались на всіх етапах</div>
                    <div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
                        <div class="overlay-inner">
                            <div class="content">
                                <span class="icon flaticon-house-outline"></span>
                                <h4><a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/">Захист ваших прав</a></h4>
                                <a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/" class="theme-btn btn-style-one">Записатися</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Lower Section -->
        <div class="lower-section">
            <div class="carousel-box">
                <div class="content">
                    <div class="single-item-carousel owl-carousel owl-theme">

                        <?php
                            $args = [
	                            'comment__in'         =>[411, 379, 439]
                            ];
	                        $comments = get_comments( $args );
	                        foreach( $comments as $comment ):

		                        $user_ID = get_comment( $comment->comment_ID )->user_id;
                            // Get the author image URL
		                        $output = get_avatar_url($user_ID);
		                        
		                        ?>
                        
                        <!-- Testimonial Block -->
                        <div class="testimonial-block">
                            <div class="inner-box">
                                <div class="testimonial-content">
                                    <span class="quote-icon flaticon-left-quote"></span>
                                    <div class="text"><?php echo $comment->comment_content ?></div>
                                    <!-- Lower Box -->
                                    <div class="lower-box">
                                        <div class="box-inner">
                                            <div class="image">
                                                <img src="<?php  echo $output ?>" alt="<?php echo $comment->comment_author ?> avatar" />
                                            </div>
                                            <h3><?php echo $comment->comment_author ?></h3>
                                            <div class="designation">Клиент</div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
	                    <?php
		                    endforeach;
	                    
	                    ?>


                    </div>
                </div>
            </div>

            <div class="image">
                <img src="/wp-content/uploads/2014/12/advokat-po-ugolovnym-delam.jpg" alt="" style="width:420px;transform: rotateY(180deg);">
            </div>

        </div>

    </div>
</section>
<!-- End Services Section Two -->

<!-- Counter Section -->
<section class="counter-section">
    <div class="icon-one wow zoomIn" data-wow-delay="250ms" data-wow-duration="1500ms" style="background-image:url(<?php $custom_style->theImgPath() ?>icons/icon-3.png)"></div>
    <div class="icon-two wow fadeInRight" data-wow-delay="500ms" data-wow-duration="1500ms" style="background-image:url(<?php $custom_style->theImgPath() ?>icons/icon-4.png)"></div>
    <div class="container">
      
        <!-- Title Box -->
        <div class="title-box">
            <div class="section-title">
                <div class="row clearfix">
                    <div class="column col-lg-6 col-md-12 col-sm-12">
                        <div class="title">Робота адвоката</div>
                        <h3> Як ми <span>працюємо?</span></h3>
                    </div>
                    <div class="column col-lg-6 col-md-12 col-sm-12">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <p>Вивчаємо всі матеріали справи та робимо глибокий правовий аналіз усіх обставин.
                                <p>Захищаємо довірителя від можливого тиску правоохоронних органів та прокуратури.
                                <p>Вибудовуємо стратегію захисту та консультуємо довірителя по всіх пунктах провадження для позитивного вирішення справи.
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <p>Розробляємо правову позицію для свідків, потерпілих, підозрюваних, підсудних чи засуджених.
                                <p>Збираємо докази, що аргументують правову позицію підозрюваного, підсудного чи засудженого.
                                <p>Супроводжуємо кримінальне провадження та захищаємо інтереси довірителя на всіх стадіях: під час допитів, слідства, досудової роботи та при розгляді справи в суді.
                                <p>Робимо все можливе, щоб перекваліфікувати статтю, зняти звинувачення, пом'якшити вирок тощо.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Fact Counter -->
        <div class="fact-counter">
            <div class="row clearfix">
             
        
                
                
                <!--Column-->
                <div class="column counter-column col-lg-3 col-md-6 col-sm-12">
                    <div class="inner wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="count-outer count-box">
                            <span class="count-text" data-speed="3000ms" data-stop="1825">1825</span>+
                            <div class="counter-title">завершених справ</div>
                        </div>
                    </div>
                </div>

                <!--Column-->
                <div class="column counter-column col-lg-3 col-md-6 col-sm-12">
                    <div class="inner wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="count-outer count-box">
                            <span class="count-text" data-speed="2000" data-stop="13548">13548</span>+
                            <div class="counter-title">годин роботи</div>
                        </div>
                    </div>
                </div>

                <!--Column-->
                <div class="column counter-column col-lg-3 col-md-6 col-sm-12">
                    <div class="inner wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="count-outer count-box">
                            <span class="count-text" data-speed="2000" data-stop="625">625</span>+
                            <div class="counter-title">складених документів</div>
                        </div>
                    </div>
                </div>

                <!--Column-->
                <div class="column counter-column col-lg-3 col-md-6 col-sm-12">
                    <div class="inner wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
                        <div class="count-outer count-box">
                            <span class="count-text" data-speed="1500" data-stop="720">720</span>+
                            <div class="counter-title">консультацій</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>


<!-- Team Section -->
<section class="team-section">
    <div class="container">

        <!-- Sec Title -->
        <div class="section-title light">
            <div class="clearfix">
                <div class="pull-left">
                    <div class="title">Адвокати</div>
                    <h3><span>ваш опора</span> в світі права!</h3>
                </div>
                <div class="pull-right">
                    <div class="text">Ваша невиновность стоит больше, чем услуги адвоката по уголовным делам!</div>
                </div>
            </div>
        </div>

        <div class="clearfix" style="display: flex;justify-content: center;">

            <!-- Team Block -->
            <div class="team-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="image">
                        <a href="#"><img src="https://www.advokat-veritas.kh.ua/wp-content/uploads/2014/01/advokat-bereznoy.jpg" alt="Адвокат Генадій Бережной" /></a>
                    </div>
                    <div class="lower-content">
                        <h3><a href="#">Генадій Бережной</a></h3>
                        <div class="designation">Адвокат</div>
                        <div class="overlay-box">
                            <div class="overlay-content">
                                <div class="title">Контати</div>
                                <ul class="social-icons">
                                    <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                                    <li><a href="#"><span class="fa fa-vimeo"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Team Block -->
            <div class="team-block col-lg-3 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <div class="image">
                        <a href="#"><img src="https://www.femida.kharkov.ua/wp-content/uploads/2018/06/Advokat-kharkov-sergey-beregnoy-min.jpg" alt="Адвокат Сергій Бережной" /></a>
                    </div>
                    <div class="lower-content">
                        <h3><a href="#">Адвокат</a></h3>
                        <div class="designation">Сергій Бережной</div>
                        <div class="overlay-box">
                            <div class="overlay-content">
                                <div class="title">Контакти</div>
                                <ul class="social-icons">
                                    <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                                    <li><a href="#"><span class="fa fa-vimeo"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

         

        </div>

    </div>
</section>
<!-- End Team Section -->

<!-- Portfolio Section -->
<section class="portfolio-section">
    <div class="container">
        <!-- Sec Title -->
        <div class="section-title centered">
            <div class="title">Практика</div>
            <h3>Ознайомтесь з нашими послугами <br> на <span>реальних</span> кейсах</h3>
        </div>
    </div>
    
    <?php
    $front_portfolio = new WP_Query($themefunc->getQueryArgs('portfolio'));
    if ( $front_portfolio->have_posts() ) : ?>
    <!-- Outer Container -->
    <div class="outer-container">
        <div class="portfolio-carousel owl-carousel owl-theme">
         <?php while($front_portfolio->have_posts()): $front_portfolio->the_post();
              $url = get_the_post_thumbnail_url( null, 'Large' );
              $alttext = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
              ?>
            <!-- Portfolio Block -->
            <div class="portfolio-block">
                <div class="inner-box">
                    <div class="image">
                        <img src="<?php echo $url ?>" alt="<?php echo $alttext ?>" />
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="content">
                                    <div class="title">Personal Law</div>
                                    <h3><?php the_title() ?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="overlay-link">
                            <a href="<?php echo $url ?>" data-fancybox="gallery-1" data-caption="" class="plus flaticon-plus"></a>
                        </div>
                    </div>
                </div>
            </div>
         <?php endwhile; ?>


        </div>

    </div>
    <?php endif; ?>

</section>
<!-- End Portfolio Section -->

<!-- Form Section -->
<section class="form-section" style="background-image: url(<?php $custom_style->theImgPath() ?>background/2.jpg)">
    <div class="container">
        <!-- Upper Content -->
        <div class="upper-content">
            <div class="row clearfix">

                <!-- Title Column -->
                <div class="title-column col-lg-5 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <!-- Sec Title -->
                        <div class="section-title light">
                            <div class="title">Звязок з нами</div>
                            <h3>Будь-які <br> запитання <span>нам</span></h3>
                        </div>
                    </div>
                </div>

                <!-- Info Column -->
                <div class="info-column col-lg-7 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <div class="row clearfix">
                            <!-- Column -->
                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                <ul class="list-style-two">
                                    <li><span class="icon flaticon-placeholder-1"></span>
                                        <?php echo CONTACT['country'] . CONTACT['separator'] . CONTACT['city'] . CONTACT['separator'] . CONTACT['full_address'] ?>
                                    </li>
                                </ul>
                            </div>
                            <!-- Column -->
                            <div class="column col-lg-6 col-md-6 col-sm-12">
                                <ul class="list-style-two">


                                    <li><span class="icon flaticon-phone-call"></span><?php echo CONTACT['tel'] ?></li>
<!--                                    <li><span class="icon flaticon-chat"></span>support@lawsight.com</li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Lower Content -->
        <div class="lower-content">

            <!-- Default Form -->
            <div class="default-form">
                <?php
                $short_code_form = '[contact-form-7 id="' . $themefunc->get_cf7_id('205045', '205046') . '"  html_id="contact-form" ]';
                echo do_shortcode( $short_code_form ); ?>
            <!--End Default Form-->

        </div>
    </div>
</section>
<!-- End Form Section -->

<!-- News Section -->
<section class="news-section">
    <div class="container">

        <!-- Sec Title -->
        <div class="section-title">
            <div class="clearfix">
                <div class="pull-left">
                    <div class="title">Блог</div>
                    <h3>Пізнайте з нами <br> останні <span>новини</span></h3>
                </div>
                <div class="pull-right">
                    <div class="text"></div>
                </div>
            </div>
        </div>

        <?php
        $front_blog = new WP_Query($themefunc->getQueryArgs('post', 3));
        if ($front_blog->have_posts()): ?>
        <div class="row clearfix">
             <?php while($front_blog->have_posts()): $front_blog->the_post(); ?>
            <!-- News Block -->
            <div class="news-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                       <?php
                        $url = get_the_post_thumbnail_url( null, 'medium' );
                        $id = get_the_ID();
                        $alt = Image::get_the_post_thumbnail_alt(get_the_ID());
                        if ( ! empty( $url ) ) { ?>
                            <div class="image">
                                <img src="<?php echo $url ?>" alt="<?php echo Image::get_the_post_thumbnail_alt(get_the_ID())??''; ?>" />
                                <div class="overlay-box">
                                    <a href="<?php echo $url ?>" data-fancybox="news" data-caption="" class="plus flaticon-plus"></a>
                                </div>
                            </div>
                            <?php
                        }
                       ?>
                    <div class="lower-content">
                        <ul class="post-meta">
                            <li><span class="fa fa-calendar"></span><?php echo get_the_date() ?></li>
                            <li><span class="fa fa-user"></span><?php echo get_the_author() ?></li>
                        </ul>
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h5>
                        <a href="<?php the_permalink(); ?>" class="theme-btn btn-style-two"><?php _e('Читати', 'veritas'); ?></a>
                    </div>
                </div>
            </div>
             <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>


</section>
<!-- End News Section -->

<!-- Subscribe Section -->
<?php // get_template_part('template-parts/subscription') ?>
<!-- End Subscribe Section -->

<?php get_footer('law'); ?>

