<?php
use  AstiDivi\Classes\Image;
    global $custom_style;
    get_header('law');
    global $cat;
//    $category = get_the_category();


    ?>

    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php $custom_style ->theImgPath() ?>background/4.jpg)">
        <div class="container">
            <div class="content">
                <h1><?php echo get_cat_name( $cat ) ?></h1>
                <ul class="page-breadcrumb">
                    <li><a href="/"><?php _e('Головна', ''); ?></a></li>
                    <li><?php echo get_cat_name( $cat ) ?></li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Blog Page Section -->
    <section class="blog-page-section">
        <div class="container">

            <?php if (have_posts()): ?>

            <div class="row clearfix">

                <?php while(have_posts()): the_post(); ?>
                <!-- News Block -->
                <div class="news-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <?php
                        $url = get_the_post_thumbnail_url( null, 'medium' );
                        $id = get_the_ID();
                        $alt = Image::get_the_post_thumbnail_alt(get_the_ID());
                        if ( ! empty( $url ) ) { ?>
                            <div class="image">
                                <img src="<?php echo $url ?>" alt="<?php echo Image::get_the_post_thumbnail_alt(get_the_ID())??''; ?>" />
                                <div class="overlay-box">
                                    <a href="<?php echo $url ?>" data-fancybox="news" data-caption="" class="plus flaticon-plus"></a>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="lower-content">
                            <ul class="post-meta">
                                <li><span class="fa fa-calendar"></span><?php echo get_the_date() ?></li>
                                <li><span class="fa fa-user"></span><?php echo get_the_author() ?></li>
                            </ul>
                            <h5><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h5>
                            <a href="<?php the_permalink(); ?>" class="theme-btn btn-style-three"><?php _e('Читати', ''); ?></a>
                        </div>
                    </div>
                </div>

                <?php endwhile; ?>


            </div>
            <?php endif; ?>


        </div>
    </section>
    <!-- End Blog Page Section -->

    <!-- Subscribe Section -->
     <?php get_template_part('template-parts/subscription') ?>
    <!-- End Subscribe Section -->

    <!--Main Footer-->
    <footer class="main-footer">
        <div class="container">
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">

                    <!--Column-->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
                        <div class="row clearfix">

                            <!--Footer Column-->
                            <div class="footer-column col-lg-7 col-md-6 col-sm-12">
                                <div class="footer-widget logo-widget">
                                    <div class="logo">
                                        <a href="index.html"><img src="images/footer-logo.png" alt="" /></a>
                                    </div>
                                    <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</div>
                                    <ul class="list-style-three">
                                        <li><span class="icon fa fa-phone"></span> +123 (4567) 890</li>
                                        <li><span class="icon fa fa-envelope"></span> info@lawsight.com</li>
                                        <li><span class="icon fa fa-home"></span>380 St Kilda Road, Melbourne <br> VIC 3004, Australia</li>
                                    </ul>
                                </div>
                            </div>

                            <!--Footer Column-->
                            <div class="footer-column col-lg-5 col-md-6 col-sm-12">
                                <div class="footer-widget links-widget">
                                    <h4>Links</h4>
                                    <ul class="list-link">
                                        <li><a href="">Home</a></li>
                                        <li><a href="">Services</a></li>
                                        <li><a href="">About us</a></li>
                                        <li><a href="">Testimonials</a></li>
                                        <li><a href="">News</a></li>
                                        <li><a href="">Contact</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--Column-->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
                        <div class="row clearfix">

                            <!--Footer Column-->
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                <div class="footer-widget links-widget">
                                    <h4>Support</h4>
                                    <ul class="list-link">
                                        <li><a href="">Contact Us</a></li>
                                        <li><a href="">Submit a Ticket</a></li>
                                        <li><a href="">Visit Knowledge Base</a></li>
                                        <li><a href="">Support System</a></li>
                                        <li><a href="">Refund Policy</a></li>
                                        <li><a href="">Professional Services</a></li>
                                    </ul>
                                </div>
                            </div>

                            <!--Footer Column-->
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                <div class="footer-widget gallery-widget">
                                    <h4>Gallery</h4>
                                    <div class="widget-content">
                                        <div class="images-outer clearfix">
                                            <!--Image Box-->
                                            <figure class="image-box"><a href="images/gallery/1.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-1.jpg" alt=""></a></figure>
                                            <!--Image Box-->
                                            <figure class="image-box"><a href="images/gallery/2.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-2.jpg" alt=""></a></figure>
                                            <!--Image Box-->
                                            <figure class="image-box"><a href="images/gallery/3.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-3.jpg" alt=""></a></figure>
                                            <!--Image Box-->
                                            <figure class="image-box"><a href="images/gallery/4.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-4.jpg" alt=""></a></figure>
                                            <!--Image Box-->
                                            <figure class="image-box"><a href="images/gallery/5.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-5.jpg" alt=""></a></figure>
                                            <!--Image Box-->
                                            <figure class="image-box"><a href="images/gallery/6.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-6.jpg" alt=""></a></figure>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row clearfix">

                    <!-- Copyright Column -->
                    <div class="copyright-column col-lg-6 col-md-6 col-sm-12">
                        <div class="copyright">2019 &copy; All rights reserved by <a href="#">Themexriver</a></div>
                    </div>

                    <!-- Social Column -->
                    <div class="social-column col-lg-6 col-md-6 col-sm-12">
                        <ul>
                            <li class="follow">Follow us: </li>
                            <li><a href="#"><span class="fa fa-facebook-square"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter-square"></span></a></li>
                            <li><a href="#"><span class="fa fa-linkedin-square"></span></a></li>
                            <li><a href="#"><span class="fa fa-google-plus-square"></span></a></li>
                            <li><a href="#"><span class="fa fa-rss-square"></span></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </footer>

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-circle-up"></span></div>

<!--<script src="js/jquery.js"></script>-->
<!--<script src="js/popper.min.js"></script>-->
<!--<script src="js/bootstrap.min.js"></script>-->
<!--<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>-->
<!--<script src="js/jquery.fancybox.js"></script>-->
<!--<script src="js/appear.js"></script>-->
<!--<script src="js/isotope.js"></script>-->
<!--<script src="js/owl.js"></script>-->
<!--<script src="js/wow.js"></script>-->
<!--<script src="js/jquery-ui.js"></script>-->
<!--<script src="js/script.js"></script>-->
<?php wp_footer(); ?>
</body>
</html>