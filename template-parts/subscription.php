<?php global $custom_style ; ?>
<section class="subscribe-section style-two">
    <div class="container">
        <div class="inner-container" style="background-image: url(<?php $custom_style ->theImgPath() ?>background/3.jpg)">
            <h2>Subscribe Your Email for Newsletter <br> & Promotion</h2>
            <!-- Subscribe Form -->
            <div class="subscribe-form">
                <form method="post" action="contact">
                    <div class="form-group">
                        <input type="email" name="email" value="" placeholder="Email address.." required>
                        <button type="submit" class="theme-btn subscribe-btn">Subscribe</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>