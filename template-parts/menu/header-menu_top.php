<?php
wp_nav_menu( [
//    'menu'            => 'menu-1',
    'theme_location'  => 'primary-menu',
//    'container'       => 'nav',
    'container_class' => 'navbar-collapse collapse clearfix',
    'container_id'    => 'navbarSupportedContent1',
    'menu_class'      => 'navigation clearfix',
    'menu_id'         => false,
    'echo'            => true,
    'fallback_cb'     => 'wp_page_menu',
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => '',
    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    'depth'           => 0,
    'walker'          => '',
] );