<section class="pricing-section">
	<div class="container">
		<!-- Sec Title -->
		<div class="section-title centered">
			<div class="title">Pricing</div>
			<h3>Choose the best pricing <br> to start <span> consulting </span></h3>
		</div>
		
		<!--Pricing Info Tabs-->
		<div class="pricing-info-tabs">
			<!--Pricing Tabs-->
			<div class="pricing-tabs tabs-box">
				
				<!--Tab Btns-->
				<ul class="tab-btns tab-buttons clearfix">
					<li data-tab="#package-monthly" class="tab-btn active-btn"><span class="circle"></span> Monthly</li>
					<li data-tab="#package-yearly" class="tab-btn"><span class="circle"></span> Yearly <span class="save">Save 20%</span></li>
				</ul>
				
				<!--Tabs Container-->
				<div class="tabs-content">
					
					<!--Tab / Active Tab-->
					<div class="tab active-tab" id="package-monthly">
						<div class="content">
							<div class="row clearfix">
								
								<!-- Price Block -->
								<div class="price-block col-lg-4 col-md-6 col-sm-12">
									<div class="inner-box">
										<!-- Title Box -->
										<div class="title-box">
											<div class="icon flaticon-startup-1"></div>
											<h5>Basic Plan</h5>
										</div>
										<div class="price">$29 <span>/ monthly</span></div>
										<div class="lower-box">
											<ul class="price-list">
												<li>Perspiciatis unde omnis iste natus</li>
												<li>error sit volupta tem accusantium</li>
												<li>doloremque laudantium, totam rem</li>
												<li>aperiam, eaque ipsa quae ab illo </li>
												<li>inventore veritatis.</li>
											</ul>
											<a href="#" class="theme-btn btn-style-two">Get Started</a>
										</div>
									</div>
								</div>
								
								<!-- Price Block -->
								<div class="price-block col-lg-4 col-md-6 col-sm-12">
									<div class="inner-box">
										<!-- Title Box -->
										<div class="title-box">
											<div class="icon flaticon-startup"></div>
											<h5>Standard Plan</h5>
										</div>
										<div class="price">$49 <span>/ monthly</span></div>
										<div class="lower-box">
											<ul class="price-list">
												<li>Perspiciatis unde omnis iste natus</li>
												<li>error sit volupta tem accusantium</li>
												<li>doloremque laudantium, totam rem</li>
												<li>aperiam, eaque ipsa quae ab illo </li>
												<li>inventore veritatis.</li>
											</ul>
											<a href="#" class="theme-btn btn-style-two">Get Started</a>
										</div>
									</div>
								</div>
								
								<!-- Price Block -->
								<div class="price-block col-lg-4 col-md-12 col-sm-12">
									<div class="inner-box">
										<!-- Title Box -->
										<div class="title-box">
											<div class="icon flaticon-startup-2"></div>
											<h5>Extended Plan</h5>
										</div>
										<div class="price">$59 <span>/ monthly</span></div>
										<div class="lower-box">
											<ul class="price-list">
												<li>Perspiciatis unde omnis iste natus</li>
												<li>error sit volupta tem accusantium</li>
												<li>doloremque laudantium, totam rem</li>
												<li>aperiam, eaque ipsa quae ab illo </li>
												<li>inventore veritatis.</li>
											</ul>
											<a href="#" class="theme-btn btn-style-two">Get Started</a>
										</div>
									</div>
								</div>
							
							</div>
						</div>
					</div>
					
					<!-- Tab -->
					<div class="tab" id="package-yearly">
						<div class="content">
							
							<div class="row clearfix">
								
								<!-- Price Block -->
								<div class="price-block col-lg-4 col-md-6 col-sm-12">
									<div class="inner-box">
										<!-- Title Box -->
										<div class="title-box">
											<div class="icon flaticon-startup-1"></div>
											<h5>Basic Plan</h5>
										</div>
										<div class="price">$29 <span>/ monthly</span></div>
										<div class="lower-box">
											<ul class="price-list">
												<li>Perspiciatis unde omnis iste natus</li>
												<li>error sit volupta tem accusantium</li>
												<li>doloremque laudantium, totam rem</li>
												<li>aperiam, eaque ipsa quae ab illo </li>
												<li>inventore veritatis.</li>
											</ul>
											<a href="#" class="theme-btn btn-style-two">Get Started</a>
										</div>
									</div>
								</div>
								
								<!-- Price Block -->
								<div class="price-block col-lg-4 col-md-6 col-sm-12">
									<div class="inner-box">
										<!-- Title Box -->
										<div class="title-box">
											<div class="icon flaticon-startup"></div>
											<h5>Standard Plan</h5>
										</div>
										<div class="price">$49 <span>/ monthly</span></div>
										<div class="lower-box">
											<ul class="price-list">
												<li>Perspiciatis unde omnis iste natus</li>
												<li>error sit volupta tem accusantium</li>
												<li>doloremque laudantium, totam rem</li>
												<li>aperiam, eaque ipsa quae ab illo </li>
												<li>inventore veritatis.</li>
											</ul>
											<a href="#" class="theme-btn btn-style-two">Get Started</a>
										</div>
									</div>
								</div>
								
								<!-- Price Block -->
								<div class="price-block col-lg-4 col-md-12 col-sm-12">
									<div class="inner-box">
										<!-- Title Box -->
										<div class="title-box">
											<div class="icon flaticon-startup-2"></div>
											<h5>Extended Plan</h5>
										</div>
										<div class="price">$59 <span>/ monthly</span></div>
										<div class="lower-box">
											<ul class="price-list">
												<li>Perspiciatis unde omnis iste natus</li>
												<li>error sit volupta tem accusantium</li>
												<li>doloremque laudantium, totam rem</li>
												<li>aperiam, eaque ipsa quae ab illo </li>
												<li>inventore veritatis.</li>
											</ul>
											<a href="#" class="theme-btn btn-style-two">Get Started</a>
										</div>
									</div>
								</div>
							
							</div>
						
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</div>
</section>
