<?php
global $custom_style;

$add_css_class = $args['additinal_css_class'] ?? '';

	?>
<div class="row clearfix">
	
	<!-- Services Block Two -->
	<div class="services-block-two col-lg-4 col-md-6 col-sm-12 <?php echo $add_css_class ?>">
		<div class="inner-box">
			<div class="icon-box">
				<span class="icon flaticon-internet"></span>
			</div>
			<h3>Адвокат по наркотиках</h3>
			<div class="text">Допомагаємо у вирішенні кримінальних проваджень за ст. 307 УК України</div>
			<div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
				<div class="overlay-inner">
					<div class="content">
						<span class="icon flaticon-internet"></span>
						<h4><a href="<?php echo get_home_url(null, '/') ?>/advokat-kharkov-service/advokat-po-narkotikam-kharkov.html">Адвокат по наркотиках</a></h4>
						<a href="<?php echo get_home_url(null, '/') ?>/advokat-kharkov-service/advokat-po-narkotikam-kharkov.html" class="theme-btn btn-style-one">Записатися</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Services Block Two -->
	<div class="services-block-two col-lg-4 col-md-6 col-sm-12 <?php echo $add_css_class ?>">
		<div class="inner-box">
			<div class="icon-box">
				<span class="icon flaticon-museum"></span>
			</div>
			<h3>Цивільне право</h3>
			<div class="text">Захищаємо та представляємо вашу сторону у суперечках з приватними або юридичними особами та державою</div>
			<div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
				<div class="overlay-inner">
					<div class="content">
						<span class="icon flaticon-museum"></span>
						<h4><a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/">Цивільне право</a></h4>
						<a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/" class="theme-btn btn-style-one">Записатися</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Services Block Two -->
	<div class="services-block-two col-lg-4 col-md-6 col-sm-12 <?php echo $add_css_class ?>">
		<div class="inner-box">
			<div class="icon-box">
				<span class="icon flaticon-gun"></span>
			</div>
			<h3>Крадіжки й пограбування</h3>
			<div class="text">Надаємо консультацію за всіма запитаннями, що стосуються справи. Захищаємо ваші інтереси в суді</div>
			<div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
				<div class="overlay-inner">
					<div class="content">
						<span class="icon flaticon-gun"></span>
						<h4><a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/">Крадіжки й пограбування</a></h4>
						<a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/" class="theme-btn btn-style-one">Записатися</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Services Block Two -->
	<div class="services-block-two col-lg-4 col-md-6 col-sm-12 <?php echo $add_css_class ?>">
		<div class="inner-box">
			<div class="icon-box">
				<span class="icon flaticon-plan"></span>
			</div>
			<h3>Особисті консультації</h3>
			<div class="text">Ми — ваша надійна опора в розв'язанні будь-яких юридичних питань</div>
			<div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
				<div class="overlay-inner">
					<div class="content">
						<span class="icon flaticon-plan"></span>
						<h4><a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/">Особисті консультації</a></h4>
						<a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/" class="theme-btn btn-style-one">Записатися</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Services Block Two -->
	<div class="services-block-two col-lg-4 col-md-6 col-sm-12 <?php echo $add_css_class ?>">
		<div class="inner-box">
			<div class="icon-box">
				<span class="icon flaticon-book"></span>
			</div>
			<h3>Виконавчі провадження</h3>
			<div class="text">Допомога в стягненні заборгованості</div>
			<div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
				<div class="overlay-inner">
					<div class="content">
						<span class="icon flaticon-book"></span>
						<h4><a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/">Виконавчі провадження</a></h4>
						<a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/" class="theme-btn btn-style-one">Записатися</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Services Block Two -->
	<div class="services-block-two col-lg-4 col-md-6 col-sm-12 <?php echo $add_css_class ?>">
		<div class="inner-box">
			<div class="icon-box">
				<span class="icon flaticon-house-outline"></span>
			</div>
			<h3>Захист ваших прав</h3>
			<div class="text">Адвокат пильно вивчає всі матеріали справи та стежить, щоб ваші права не порушувались на всіх етапах</div>
			<div class="overlay-box" style="background-image: url(<?php $custom_style->theImgPath() ?>resource/service-1.jpg);">
				<div class="overlay-inner">
					<div class="content">
						<span class="icon flaticon-house-outline"></span>
						<h4><a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/">Захист ваших прав</a></h4>
						<a href="<?php echo get_home_url(null, '/') ?>/services/yuridicheskaya-konsultatsiya-advokata-po-ugolovnyim-delam/" class="theme-btn btn-style-one">Записатися</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
