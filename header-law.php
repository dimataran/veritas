<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <?php
    elegant_description();
    elegant_keywords();
    elegant_canonical();

    /**
     * Fires in the head, before {@see wp_head()} is called. This action can be used to
     * insert elements into the beginning of the head before any styles or scripts.
     *
     * @since 1.0
     */
    do_action( 'et_head_meta' );

    $template_directory_uri = get_template_directory_uri();
    ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
    <?php wp_head(); ?>
</head>

<body>

<div class="page-wrapper">

    <!-- Preloader -->
<!--        <div class="preloader"></div>-->

    <!-- Main Header-->
    <header class="main-header">

        <!--Header-Upper-->
        <div class="header-upper">
            <div class="container">
                <div class="clearfix">
                    <?php
                    global $logo, $logo_width, $logo_height;
                    $logo = ( $user_logo = et_get_option( 'divi_logo' ) ) && ! empty( $user_logo )
                        ? $user_logo
                        : $template_directory_uri . '/images/logo.png';

                    // Get logo image size based on attachment URL.
                    $logo_size   = et_get_attachment_size_by_url( $logo );
                    $logo_width  = ( ! empty( $logo_size ) && is_numeric( $logo_size[0] ) )
                        ? $logo_size[0]
                        : '93'; // 93 is the width of the default logo.
                    $logo_height = ( ! empty( $logo_size ) && is_numeric( $logo_size[1] ) )
                        ? $logo_size[1]
                        : '43'; // 43 is the height of the default logo.
                    ?>

                    <div class="pull-left logo-box">
                        <div class="logo">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="img-responsive">
                                <img src="<?php echo esc_attr( $logo ); ?>" width="<?php echo esc_attr( $logo_width ); ?>" height="<?php echo esc_attr( $logo_height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="main-logo" data-height-percentage="<?php echo esc_attr( et_get_option( 'logo_height', '54' ) ); ?>" />
                            </a>
                        </div>
                    </div>

                    <div class="nav-outer clearfix">

                        <!-- Main Menu -->
                        <nav class="main-menu navbar-expand-md">
                            <div class="navbar-header">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Add Desc Menu both               -->
                            <?php get_template_part('template-parts/menu/header','menu'); ?>

                        </nav>

                        <div class="outer-box">

                            <!--Search Box-->
                            <div class="search-box-outer">
                                <div class="dropdown">
                                    <button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-search"></span></button>
                                    <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu1">
                                        <li class="panel-outer">
                                            <div class="form-container">
                                                <form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                                    <div class="form-group">
<!--                                                        <input type="search" name="field-name" value="" placeholder="Search Here" required>-->
                                                        <?php
                                                        printf( '<input type="search" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
                                                            esc_attr__( 'Пошук &hellip;', 'veritas' ),
                                                            get_search_query(),
                                                            esc_attr__( 'Пошук:', 'veritas' )
                                                        );

                                                        /**
                                                         * Fires inside the search form element, just before its closing tag.
                                                         *
                                                         * @since ??
                                                         */
                                                        do_action( 'et_search_form_fields' );
                                                        ?>

                                                        <button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
                                                    </div>
                                                </form>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!--Nav Toggler-->
                            <div class="nav-toggler"><div class="nav-btn hidden-bar-opener"><span class="flaticon-menu"></span></div></div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <!--End Header Upper-->

        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="container clearfix">
                <!--Logo-->

                <div class="logo pull-left">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="img-responsive">
                        <img src="<?php echo esc_attr( $logo ); ?>" width="<?php echo esc_attr( $logo_width ); ?>" height="<?php echo esc_attr( $logo_height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="sticky-logo" data-height-percentage="<?php echo esc_attr( et_get_option( 'logo_height', '54' ) ); ?>" />
                    </a>
                </div>

                <!--Right Col-->
                <div class="right-col pull-right">
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <?php get_template_part('template-parts/menu/header','menu_top'); ?>

                    </nav><!-- Main Menu End-->
                </div>

            </div>
        </div>
        <!--End Sticky Header-->

    </header>
    <!--End Main Header -->

    <!--Form Back Drop-->
    <div class="form-back-drop"></div>

    <!-- Hidden Navigation Bar -->
    <section class="hidden-bar right-align">
        <div class="hidden-bar-closer">
            <button><span class="fa fa-remove"></span></button>
        </div>
        <!-- Hidden Bar Wrapper -->
        <div class="hidden-bar-wrapper">
            <div class="inner-box">
                <div class="logo">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo esc_attr( $logo ); ?>" width="<?php echo esc_attr( $logo_width ); ?>" height="<?php echo esc_attr( $logo_height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="hidden-logo" data-height-percentage="<?php echo esc_attr( et_get_option( 'logo_height', '54' ) ); ?>" />
                    </a>
                </div>
                <div class="text">Швидко та максимально ефективно вирішуємо будь-які питання, що стосуються права. Запишіться на консультацію вже зараз!</div>
                <!-- List Style Four -->
                <ul class="list-style-four">
                    <li><span class="icon flaticon-house"></span> <strong>Проспект Героїв Харкова,</strong> 257, оф 721, Україна</li>
                    <li><span class="icon flaticon-phone-call"></span> <strong>Телефон!</strong>063-868-58-40</li>
<!--                    <li><span class="icon flaticon-talk"></span><strong>Mail address</strong>info@domain.com</li>-->
                </ul>
                <div class="lower-box">
                    <!-- Social Icons -->
                    <ul class="social-icons">
<!--                        <li class="facebook"><a href="#"><span class="fa fa-facebook"></span></a></li>-->
<!--                        <li class="twitter"><a href="#"><span class="fa fa-twitter"></span></a></li>-->
<!--                        <li class="pinterest"><a href="#"><span class="fa fa-pinterest-p"></span></a></li>-->
<!--                        <li class="vimeo"><a href="#"><span class="fa fa-vimeo"></span></a></li>-->
                    </ul>
                    <a href="/advokat-kharkov-contacts" class="theme-btn buy-btn">Звернутись до адвоката!</a>
                </div>
            </div>
        </div><!-- / Hidden Bar Wrapper -->
    </section>
    <!-- End / Hidden Bar -->
