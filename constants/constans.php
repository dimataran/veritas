<?php
//	'portfolio_cat', array( 'portfolio'
define('SERVICE_TAX', 'service_cat');
define('SERVICE', 'service');
define('PORTFOLIO_TAX', 'portfolio_cat');
define('PORTFOLIO', 'portfolio');
define('PORTFOLIO_PER_PAGE', get_option('posts_per_page', 12));
define('CONTACT', ['separator' => ', ', 'country' => 'Україна', 'city' => 'Харків', 'full_address' => 'проспект Героїв Харкова, 257, оф 721', 'tel' => '063-868-58-40']);