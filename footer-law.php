<?php
$et_secondary_nav_items = et_divi_get_top_nav_items();

$et_phone_number = $et_secondary_nav_items->phone_number;

$et_email = $et_secondary_nav_items->email;

$et_contact_info_defined = $et_secondary_nav_items->contact_info_defined;

global $logo, $logo_width, $logo_height;

if ( et_theme_builder_overrides_layout( ET_THEME_BUILDER_HEADER_LAYOUT_POST_TYPE ) || et_theme_builder_overrides_layout( ET_THEME_BUILDER_FOOTER_LAYOUT_POST_TYPE ) ) {
    // Skip rendering anything as this partial is being buffered anyway.
    // In addition, avoids get_sidebar() issues since that uses
    // locate_template() with require_once.
    return;
}

/**
 * Fires after the main content, before the footer is output.
 *
 * @since 3.10
 */
do_action( 'et_after_main_content' );
?>

<!--Main Footer-->
<footer class="main-footer">
    <div class="container">
        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row clearfix">

                <!--Column-->
                <div class="big-column col-lg-6 col-md-12 col-sm-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-lg-7 col-md-6 col-sm-12">
                            <div class="footer-widget logo-widget">
                                <div class="logo">
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="img-responsive">
                                        <img src="<?php echo esc_attr( $logo ); ?>" width="<?php echo esc_attr( $logo_width ); ?>" height="<?php echo esc_attr( $logo_height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="footer-logo" data-height-percentage="<?php echo esc_attr( et_get_option( 'logo_height', '54' ) ); ?>" />
                                    </a>
                                </div>
                                <div class="text">
                                    <h4>Кратко об адвокате</h4>
                                        <ol>
                                            <li>Специализация – исключительно уголовные дела.</li>
                                            <li>Количество выигранных дел – 150
                                            </li>
                                            <li>Максимально быстро закрытое дело — 2 дня</li>
                                            <li>Стаж – с 2011 года</li>
                                        </ol>
                                </div>
                                <?php if ( $et_contact_info_defined ) : ?>

                                    <ul class="list-style-three">
                                    <?php if ( ! empty( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
                                        <li><span class="icon fa fa-phone"></span><?php echo et_core_esc_previously( et_sanitize_html_input_text( strval( $et_phone_number ) ) ); ?></li>
                                    <?php endif; ?>
                                    <?php if ( ! empty( $et_email = et_get_option( 'header_email' ) ) ) : ?>
                                        <li><span class="icon fa fa-envelope"></span><?php echo esc_html( $et_email ); ?></li>
                                    <?php endif; ?>
                                        <li><span class="icon fa fa-home"></span>Україна, Харків <br> Проспект Героїв Харкова, 257, оф 721</li>
                                    </ul>
                                <?php endif; // true === $et_contact_info_defined ?>

                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="footer-column col-lg-5 col-md-6 col-sm-12">
                            <div class="footer-widget links-widget">
<!--                                <h4>Links</h4>-->
<!--                                <ul class="list-link">-->
<!--                                    <li><a href="">Home</a></li>-->
<!--                                    <li><a href="">Services</a></li>-->
<!--                                    <li><a href="">About us</a></li>-->
<!--                                    <li><a href="">Testimonials</a></li>-->
<!--                                    <li><a href="">News</a></li>-->
<!--                                    <li><a href="">Contact</a></li>-->
<!--                                </ul>-->
                            </div>
                        </div>

                    </div>
                </div>

                <!--Column-->
                <div class="big-column col-lg-6 col-md-12 col-sm-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                            <div class="footer-widget links-widget">
<!--                                <h4>Support</h4>-->
<!--                                <ul class="list-link">-->
<!--                                    <li><a href="">Contact Us</a></li>-->
<!--                                    <li><a href="">Submit a Ticket</a></li>-->
<!--                                    <li><a href="">Visit Knowledge Base</a></li>-->
<!--                                    <li><a href="">Support System</a></li>-->
<!--                                    <li><a href="">Refund Policy</a></li>-->
<!--                                    <li><a href="">Professional Services</a></li>-->
<!--                                </ul>-->
                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                            <div class="footer-widget gallery-widget">
<!--                                <h4>Gallery</h4>-->
                                <div class="widget-content">
                                    <div class="images-outer clearfix">
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="images/gallery/1.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-1.jpg" alt=""></a></figure>
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="images/gallery/2.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-2.jpg" alt=""></a></figure>
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="images/gallery/3.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-3.jpg" alt=""></a></figure>
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="images/gallery/4.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-4.jpg" alt=""></a></figure>
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="images/gallery/5.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-5.jpg" alt=""></a></figure>
                                        <!--Image Box-->
                                        <figure class="image-box"><a href="images/gallery/6.jpg" class="lightbox-image" data-fancybox="footer-gallery" title="Image Title Here" data-fancybox-group="footer-gallery"><img src="images/gallery/footer-gallery-thumb-6.jpg" alt=""></a></figure>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row clearfix">

                <!-- Copyright Column -->
                <div class="copyright-column col-lg-6 col-md-6 col-sm-12">
                    <div class="copyright"><?php echo date('Y') ?> &copy; All rights reserved by <a href="/">Адвокат Бережной</a></div>
                </div>

                <!-- Social Column -->
                <div class="social-column col-lg-6 col-md-6 col-sm-12">
                    <ul>
                        <li class="follow">Follow us: </li>
                        <li><a href="#"><span class="fa fa-facebook-square"></span></a></li>
                        <li><a href="#"><span class="fa fa-twitter-square"></span></a></li>
                        <li><a href="#"><span class="fa fa-linkedin-square"></span></a></li>
                        <li><a href="#"><span class="fa fa-google-plus-square"></span></a></li>
                        <li><a href="#"><span class="fa fa-rss-square"></span></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</footer>

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-circle-up"></span></div>

<?php wp_footer(); ?>
</body>
</html>
