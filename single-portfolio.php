<?php
get_header('law');
global $themefunc, $custom_style;

   $services_cat_terms = $terms = get_terms( [
       'taxonomy' => 'service_cat',
       'hide_empty' => true,
   ] );

$post_id = get_the_ID();
$args = $themefunc->getQueryArgs( 'portfolio', 3, orderby:'title', post__not_in:[$post_id]);
$portfolio_query = new WP_Query($args);


$url = get_the_post_thumbnail_url( null, 'Large' );
    if (empty($url)) {
    $url = 'https://www.advokat-veritas.kh.ua/wp-content/uploads/2021/01/vzyatka-300x175.jpg';
    }

$alttext = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );

?>

    <section class="page-title" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(<?php echo $url ?>)">
        <div class="container">
            <div class="content">
                <h1><?php the_title() ?></h1>
                <ul class="page-breadcrumb">
                    <li><a href="/"><?php _e('Головна', ''); ?></a></li>
                    <li><?php the_title() ?></li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Portfolio Single Section -->
    <section class="portfolio-single-section">
        <div class="container">
            <!-- Sec Title -->
            <div class="section-title centered">
                <div class="title">Наші Кейси</div>
                <h3>Досвід, що <span>перемогає!</span></h3>
            </div>

            <div class="row clearfix">

                <!-- Image Column -->
                <div class="image-column col-lg-7 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <div class="image">
                            <img src="<?php echo $url ?>" alt="<?php echo $alttext ?>" />
                        </div>
                    </div>
                </div>

                <!-- Content Column -->
                <div class="content-column col-lg-5 col-md-12 col-sm-12">
                    <div class="inner-column" style="padding-top:0; padding-left:0">
                        <div class="text">
                            <?php echo apply_filters('the_content', get_the_content()) ?>
                        </div>
                        <ul class="project-list">
<!--                            <li><span class="icon fa fa-tag"></span> <strong>Category: </strong>Strategy</li>-->
<!--                            <li><span class="icon fa fa-user"></span> <strong>Client: </strong>Real Madrid C.F</li>-->
<!--                            <li><span class="icon fa fa-calendar"></span> <strong>Date: </strong>24/11/2017</li>-->
<!--                            <li><span class="icon fa fa-external-link"></span> <strong>Website: </strong>www.madridista.esp</li>-->
                        </ul>
                    </div>
                </div>

            </div>

            <!-- Lower Section -->
            <div class="lower-section">
                <div class="row clearfix">
                    
                    <?php
	                    
	                    
	                    if( $portfolio_query->have_posts() ): ?>
              
			                    <?php while($portfolio_query->have_posts() ): $portfolio_query->the_post();
				                    $add_class = '';
				                    setup_postdata($post);
				                   
                                    $img_url = get_the_post_thumbnail_url( $post );
                                    $alttext = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
				                    ?>
                                    
                                    <!-- Project Block Two -->
                                    <div class="portfolio-block-two col-lg-4 col-md-6 col-sm-12">
                                        <div class="inner-box">
                                            <div class="image">
                                                <img src="<?php echo $img_url ?>" alt="<?php echo $alttext ?>" />
                                                <div class="overlay-box">
                                                    <a href="<?php echo $img_url ?>" data-fancybox="gallery-2" data-caption="" class="plus flaticon-plus"></a>
                                                </div>
                                            </div>
                                            <div class="lower-content">
                                                <h5><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h5>
<!--                                                <div class="designation">Sustainability</div>-->
                                            </div>
                                        </div>
                                    </div>
                                    
                                 
			                    <?php
			                    
			                    endwhile;
				                    wp_reset_postdata();
			                    ?>
                            
              
	                    <?php endif ?>
                    
                    
          


                  

                </div>
            </div>

        </div>
    </section>
    <!-- End Portfolio Single Section -->


    <!-- Subscribe Section -->

    <?php //get_template_part('template-parts/subscription') ?>

    <!-- End Subscribe Section -->

    <?php get_footer('law') ?>
