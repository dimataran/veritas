<?php


namespace AstiDivi\Classes;


class ThemeFunctions
{

  
    public function __construct()
    {
    
        
    }
    
    /**
     * @param string $post_type
     * @param int $posts_per_page
     * @param string $orderby
     * @param string $order
     * @return array
     */
    public function getQueryArgs($post_type = 'post', $posts_per_page = 10, $sticky_posts = 0, $orderby = 'date', $order = 'DESC', $post__not_in = [] )
    {
        return [
            'post_type' => $post_type,
            'posts_per_page' => $posts_per_page,
            'orderby' => $orderby,
            'order' => $order ,
            'ignore_sticky_posts' => $sticky_posts,
	        'post__not_in' => $post__not_in,
        ];
    }

    public function getQueryArgsStickPost($post_type = 'post', $posts_per_page = 1, $orderby = 'date', $order = 'DESC' )
    {
        $sticky = get_option( 'sticky_posts' );
        if ( is_array($sticky) && isset( $sticky[0])) {
            foreach ($sticky as $id) {
                $sticky_post = get_post($id);
                $sticky_post_type = $sticky_post->post_type;
                if ($sticky_post_type === $post_type ) {
                    $sticky_id = $id;
                    $args = array(
                        'post_type' => $post_type,
                        'posts_per_page' => 1,
                        'post__in' => [ $sticky_id ],
                        'ignore_sticky_posts' => 1
                    );
                    break;
                } else {
    
                    $args = [
                        'post_type' => $post_type,
                        'posts_per_page' => $posts_per_page,
                        'orderby' => $orderby,
                        'order' => $order ,
                        'ignore_sticky_posts' => 1
                    ];
                    
                }
                
            }
            
           
            
        } else {
            $args = [
                'post_type' => $post_type,
                'posts_per_page' => $posts_per_page,
                'orderby' => $orderby,
                'order' => $order ,
                'ignore_sticky_posts' => 1
            ];
        }
        
        return $args;
    }
    
    public function getQueryArgsWithPostTax($post_type = 'post',  $posts_per_page = 3,  $tax = 'category', $term = '',  $orderby = 'date', $order = 'DESC')
    {
        return [
            'post_type' => $post_type,
            'posts_per_page' => $posts_per_page,
            'orderby' => $orderby,
            'order' => $order,
            'tax_query' => [
                [
                    'taxonomy' => $tax,
                    'field'    => 'slug',
                    'terms'    => $term,
                ],
            ],
        ];
    }

    public function getQueryArgsWithSort($post_type = ['post', 'video' ],
                                        $posts_per_page = 4,
                                        $orderby = 'meta_value_num',
                                        $meta_key = 'views',
                                        $order = 'DESC'

                                        )
    {
    
    return [
        'post_type' => $post_type,
        'posts_per_page' => $posts_per_page,
        'orderby' => $orderby,
        'meta_key' => $meta_key,
        'order' =>  $order,
        'ignore_sticky_posts' => 1
    ];
    }

    public function trim_content($title, $len = 100 )
    {

        $title = wp_strip_all_tags( $title );
        if ( mb_strlen( $title, 'utf-8' ) < 100 ) {
            return $title;
        } else {
            return mb_substr( $title, 0, $len, 'utf-8') . '...';
        }

    }

    public function getTheDate()
    {
        /**
         * If less than 24 hours - display XX timmar sedan
        more than 24 hours and less than or equal to 7 days - XX dagar sedan
        More than 7 days - YYYY-MM-DD
 
         */
        global $post;
        
       $post_time = get_post_time();
        $current_time = current_time('timestamp');
        $post_time_plus_7 = strtotime('+1 week', $post_time);
        
       if ( $post_time_plus_7 < $current_time ) {
           $the_date = get_the_date('Y-m-d');
       } else {
           $the_date =  human_time_diff($post_time, $current_time ) . ' sedan';
       }
      
        return  $the_date;
    }

    public function get_cf7_id($loc_id, $prod_id)
    {
        switch ($_SERVER['SERVER_NAME']) {
            case 'advokat-veritas.kh.ua': $form_id = $prod_id; break;
            case 'test.advokat-veritas.kh.ua': $form_id = $prod_id; break;
            case 'advokat-veritas.loc': $form_id = $loc_id; break;
        }
        return $form_id;
    }



    
}