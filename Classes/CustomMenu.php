<?php

namespace AstiDivi\Classes;

class CustomMenu
{

    public function changeListItemClasses()
    {
        add_filter( 'nav_menu_css_class', [$this, 'changeClasses'], 10, 4 );
        return $this;
    }


    public function changeClasses($classes, $item, $args, $depth)
    {

        if (in_array('menu-item-has-children', $classes)) {
            $classes[] = 'dropdown';
        }
        if (in_array('current-menu-item', $classes)) {
            $classes[] = 'current';
        }

        return $classes;
    }


}