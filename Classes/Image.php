<?php


namespace AstiDivi\Classes;


class Image
{
    public $wrapClass;

    public function __construct($wrapClass = ['<div>'])
    {
        $this->wrapClass = $wrapClass;
    }

    public function imgStartWrap($class)
    {
        return '<div class="' . $class . '">';
    }

    public function imgEndWrap()
    {
        return '</div>';
    }


    public function fieldImgHtml($field = 'image', $class = 'details__img')
    {
        $image = get_field($field);
        $imgHtml = '';
        if (!empty($image) && is_array($image)) {
            $imgHtml = '<img src="' . $image['url'] . '" alt="' . $image['alt'] . '">';
        } else {
            $imgHtml = '<img src="' . post_custom($field) . '" alt="' . get_the_title() . '">';
        }
        echo $this->imgStartWrap($class) . $imgHtml . $this->imgEndWrap();

    }


    public function frontImgPath()
    {
        global $style;
        return $style->img_temp_dir;
    }

    public function initShortcode()
    {
        add_shortcode( 'front_img_path', array( $this , 'frontImgPath' ) );
    }

    public function getStartHtml2Wrap()
    {
        $html = '';
        foreach ($this->wrapClass as $class) {
            $html .= "<div class='$class'>";
        }
        return $html;
    }

    public function getEndHtml2Wrap()
    {
        return '</div></div>';
    }


    public function getLazyImg($img_url, $img_alt)
    {
       return "<img class='lazy' src='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' data-src='$img_url' alt='$img_alt'>";
    }

 

    public static function postImage( $id, $class = 'post__item-img' )
    {
        if ( $img_url = get_the_post_thumbnail_url( $id ) ) : ?>

            <div class="<?php echo $class ?>-wrap">
                <div class="<?php echo $class ?>">
                    <img class="lazy" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="<?php echo $img_url ?>" alt="<?php self::the_post_thumbnail_alt( $id );?>">
                </div>
            </div>
        <?php endif;

    }


    public static function theImage( $id, $class = 'cards__list-item-img')
    {
        if ( $img_url = get_the_post_thumbnail_url( $id ) ) : ?>

            <span class="<?php echo $class ?>">
				<img class="lazy" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="<?php echo $img_url ?>" alt="<?php self::the_post_thumbnail_alt( $id );?>">
			</span>
        <?php endif;
    }


    public static function singleImage( $id )
    {
        if ( $img_url = get_the_post_thumbnail_url( $id, 'large' ) ) : ?>

            <div class="news-single__content-img-wrap third-top-offset">
                <div class="news-single__content-img">
                    <img class="lazy" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="<?php echo $img_url ?>" alt="<?php self::the_post_thumbnail_alt( $id );?>">
                </div>
            </div>
        <?php endif;
    }


    public static function the_post_thumbnail_alt( $post_id )
    {
        $alt = self::get_the_post_thumbnail_alt( $post_id );
        if ( empty( $alt ) ) {
            $alt = get_option( 'blogname' );
        }
        echo $alt;
    }


    public static function get_the_post_thumbnail_alt( $post_id )
    {
        return get_post_meta( get_post_thumbnail_id( $post_id ), '_wp_attachment_image_alt', true);
    }
    
    
    public function getSimpleImgHtml($id)
    {
        if ( $img_url = get_the_post_thumbnail_url() ) : ?>
            <img class="w-100 h-100 article-img" src="<?php echo $img_url ?>"  alt="<?php $this->the_post_thumbnail_alt( $id );?>">
        <?php endif;
    }
    
    public function getImgPopularHtmlWithLink($post, $class = 'tumb-wrap')
    {
        if ($post->post_type === 'video') {
            $this->getImgVideoHtmlWithLink( $post->ID, $class );
        } else {
            $this->getImgHtmlWithLink( $post->ID, $class );
        }
    }
    
    public function getImgPopularHtml($post, $class = 'tumb-wrap')
    {
        if ($post->post_type === 'video') {
            $this->getImgVideoHtml( $post->ID, $class );
        } else {
            $this->getImgHtml( $post->ID, $class );
        }
    }
    
    public function getImgHtmlWithLink($id, $class = 'tumb-wrap')
    {
        if ( $img_url = get_the_post_thumbnail_url() ) : ?>
            <div class="<?php echo $class ?>">
                <a class="post_link" href="<?php the_permalink(); ?>">
                    <img class="w-100 h-100 article-img" src="<?php echo $img_url ?>"  alt="<?php $this->the_post_thumbnail_alt( $id );?>">
                </a>
            </div>
        <?php endif;
    }
    
    public function getImgHtml($id, $class = 'tumb-wrap')
    {
        if ( $img_url = get_the_post_thumbnail_url() ) : ?>
            <div class="<?php echo $class ?>">
                <img class="w-100 h-100 article-img" src="<?php echo $img_url ?>"  alt="<?php $this->the_post_thumbnail_alt( $id );?>">
            </div>
        <?php endif;
    }
    
    public function getHeroImgVideoHtml($id)
    {
        global $style;
        if ( $img_url = get_the_post_thumbnail_url() ) : ?>
            <div class="h-100 tumb-wrap">
                <img class="w-100 h-100 hero-img" src="<?php echo $img_url ?>"  alt="<?php $this->the_post_thumbnail_alt( $id );?>">
                <img class="video-icon" src="<?= $style->getImgPath(); ?>video/img_1.png" alt="Icon Circle">
                <img class="icon-tre" src="<?= $style->getImgPath(); ?>video/img_2.png" alt="Icon Triangular">
            </div>
        <?php endif;
    }
    
    
    public function getImgVideoHtml($id, $class = 'col-lg-5 tumb-wrap')
    {
        global $style;

        if ( $img_url = get_the_post_thumbnail_url() ) : ?>
            <div class="<?php echo $class ?>">
                <img class="video-icon" src="<?= $style->getImgPath(); ?>video/img_1.png" alt="Icon Circle">
                <img class="icon-tre" src="<?= $style->getImgPath(); ?>video/img_2.png" alt="Icon Triangular">
                <img class="w-100 h-100 article-img" src="<?php echo $img_url ?>"  alt="<?php $this->the_post_thumbnail_alt( $id );?>">
            </div>
        <?php endif;
    }
    
    public function getImgVideoHtmlWithLink($id, $class = 'col-lg-5 tumb-wrap')
    {
        global $style;
        
        if ( $img_url = get_the_post_thumbnail_url() ) : ?>
            <div class="<?php echo $class ?>">
                <a class="post_link" href="<?php the_permalink(); ?>">
                <img class="video-icon" src="<?= $style->getImgPath(); ?>video/img_1.png" alt="Icon Circle">
                <img class="icon-tre" src="<?= $style->getImgPath(); ?>video/img_2.png" alt="Icon Triangular">
                <img class="w-100 h-100 article-img" src="<?php echo $img_url ?>"  alt="<?php $this->the_post_thumbnail_alt( $id );?>">
                </a>
            </div>
        <?php endif;
    }
    
    
}