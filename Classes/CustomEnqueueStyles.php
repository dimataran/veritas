<?php

namespace AstiDivi\Classes;

class CustomEnqueueStyles
{

    protected $temp_dir;
    protected $css_dir;
    protected $js_dir;


    public function __construct($img_temp_dir = '/assets/images/', $css_dir = '/assets/css/', $js_dir = '/assets/js/' )
    {
        $this->temp_dir = get_stylesheet_directory_uri();
        $this->img_temp_dir = get_stylesheet_directory_uri() . $img_temp_dir;
        $this->css_dir = get_stylesheet_directory_uri() . $css_dir;
        $this->js_dir = get_stylesheet_directory_uri() . $js_dir;
        add_action( 'wp_enqueue_scripts', array($this, 'deleteScript'), 99 );
        add_action( 'wp_enqueue_scripts', array($this, 'addStyle'), 10 );
    }

    public function loadJS($js_paths, $pref = '')
    {
        if($pref !== '' ){
            $pref = '-' . $pref;
        }
        foreach ($js_paths as $js_path) {
                wp_enqueue_script($js_path . $pref,  $this->js_dir . $js_path . '.js', [], false, true);
        }

    }

//    public function addStyle()
//    {
//
//            //CSS
//            $css_paths = ['main'];
//            foreach ($css_paths as $css_path) {
//                wp_enqueue_style($css_path,  $this->css_dir . $css_path . '.css', array());
//            }
//
//            //JS
//            $this->loadJS(['main.min']);
//
//
//    }

    public function loadCSS(array $css_paths)
    {
        foreach ($css_paths as $css_path) {
            wp_enqueue_style($css_path,  $this->css_dir . $css_path . '.css', array());
        }

    }
    public function addStyle()
    {

        if (is_single() || is_category() || is_archive() || is_page() ) {
            $this->loadCSS(['bootstrap', 'main', 'responsive']);

            $script_handles = ['jquery', 'popper.min', 'bootstrap.min', 'jquery.mCustomScrollbar.concat.min',
                'jquery.fancybox', 'appear', 'isotope', 'owl', 'mixitup', 'wow', 'jquery-ui', 'script'];
            $this->loadJS($script_handles);

            // Adjust script tag attributes.
//            add_filter( 'script_loader_tag', function ( $tag, $handle, $src ) use ($script_handles) {
//                if ( in_array( $handle, $script_handles ) ) {
//                    // Add async attribute to the script tags with the src attribute.
//                    $tag = preg_replace( '/ src=/', ' async src=', $tag, 1 );
//                }
//                return $tag;
//            }, 10, 3 );
        }

    }

    public function getImgPath()
    {
        return $this->img_temp_dir;
    }

    public function theImgPath()
    {
        echo $this->getImgPath();
    }

    public function deleteScript()
    {

        if ( ! is_page('contact') ) {
            wp_dequeue_style( 'contact-form-7' );
            wp_dequeue_script( 'contact-form-7' );
        }

    }

    public function addCartScript()
    {
//    case 'wc-cart':
				$params = array(
                    'ajax_url'                     => WC()->ajax_url(),
                    'wc_ajax_url'                  => \WC_AJAX::get_endpoint( '%%endpoint%%' ),
                    'update_shipping_method_nonce' => wp_create_nonce( 'update-shipping-method' ),
                    'apply_coupon_nonce'           => wp_create_nonce( 'apply-coupon' ),
                    'remove_coupon_nonce'          => wp_create_nonce( 'remove-coupon' ),
                );
//



    }



    public function custom_manage_woo_styles() {

        if ( function_exists( 'is_woocommerce' ) ) {

            if ( ! is_front_page() && ! is_page() && ! is_woocommerce() && ! is_cart() && ! is_checkout() && ! is_account_page() || is_page('brands')) {

//                wp_dequeue_style( 'woocommerce-layout' );
//                wp_dequeue_style( 'woocommerce-smallscreen' );
//                wp_dequeue_style( 'woocommerce-general' );
                wp_dequeue_style( 'evolution-woostyles' );
                wp_dequeue_script( 'wc_price_slider' );
                wp_dequeue_script( 'wc-single-product' );
                wp_dequeue_script( 'wc-add-to-cart' );
                wp_dequeue_script( 'wc-cart-fragments' );
                wp_dequeue_script( 'wc-checkout' );
                wp_dequeue_script( 'wc-add-to-cart-variation' );
//                wp_dequeue_script( 'wc-single-product' );
                wp_dequeue_script( 'wc-cart' );
                wp_dequeue_script( 'wc-chosen' );
                wp_dequeue_script( 'woocommerce' );
                wp_dequeue_script( 'prettyPhoto' );
                wp_dequeue_script( 'prettyPhoto-init' );
                wp_dequeue_script( 'jquery-blockui' );
                wp_dequeue_script( 'jquery-placeholder' );
                wp_dequeue_script( 'fancybox' );
                wp_dequeue_script( 'jqueryui' );

            }

        }
    }


    public function add_mobile_search(){

        if (is_shop() || is_archive() ||  is_front_page()) {
            ?>
            <div class="search-mobile">
                <form role="search" method="get" class="et-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <?php
                    printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
                        esc_attr__( 'Search &hellip;', 'Divi' ),
                        get_search_query(),
                        esc_attr__( 'Search for:', 'Divi' )
                    );
                    ?>
                </form>
            </div>
        <?php }
    }

}

