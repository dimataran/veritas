<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

require_once('vendor/autoload.php');

require_once 'constants/constans.php';

/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    require_once 'woo_functions.php';
}

if ( file_exists(get_stylesheet_directory() . '/' . 'divi_functions.php' ) ) {
    require_once 'divi_functions.php';
}