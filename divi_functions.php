<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

include_once 'post_type/services-post-type.php';
include_once 'post_type/services_tax.php';
include_once 'post_type/portfolio-post-type.php';

use DiviClasses\Options;
use AstiDivi\Classes\ThemeFunctions;
use AstiDivi\Classes\CustomMenu;
use AstiDivi\Classes\Image;

$custom_menu = new CustomMenu();

$custom_menu->changeListItemClasses();
$options = new Options();
$options->init();
$themefunc = new ThemeFunctions();
$image = new Image();

add_filter('previous_post_link', 'add_css_class_to_previous_post_link');
function add_css_class_to_previous_post_link($output) {
    $output = str_replace( '<a ', '<a class="prev-post pull-left" ', $output );
    return $output;
}

add_filter('next_post_link', 'add_css_class_to_next_post_link');
function add_css_class_to_next_post_link($output) {
    $output = str_replace( '<a ', '<a class="prev-post pull-right" ', $output );
    return $output;
}

function veritas_sctipts_init() {
    global $wp_query;
    wp_localize_script( 'script', 'veritasAjax',
        array(
            'url' => admin_url('admin-ajax.php'),
            'redirecturl' => $_SERVER['REQUEST_URI'],
            'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
            'current_page' => ( get_query_var( 'paged' ) ? get_query_var('paged') : 1 ),
            'max_page' => $wp_query->max_num_pages,
            'nonce' => wp_create_nonce('myajax-nonce'),
            'nonceAappoint' => wp_create_nonce('appoint-nonce'),
            'nonceDel' => wp_create_nonce('class-del-nonce'),
            'nonceUser' => wp_create_nonce('user-nonce'),
            'ajax_url' =>  admin_url('admin-ajax.php'),

         
        )
    );
}
add_action('wp_enqueue_scripts', 'veritas_sctipts_init');


function lazyloadportfolio_ajax_handler(){
	global $image;
	global $query;
    global $themefunc;
	
    $paged = (int)sanitize_text_field($_POST['page']);
	$paged++;
//	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
//    var_dump($page);
			$args = [
				'post_type' => PORTFOLIO,
				'posts_per_page' => PORTFOLIO_PER_PAGE,
				'orderby' => 'date',
				'order' => 'DESC',
				'paged'         =>   $paged,
			];
	
//	$args = $themefunc->getQueryArgs( PORTFOLIO, PORTFOLIO_PER_PAGE, orderby:'title', post__not_in:[$post_id]);
//	$portfolio_query = new WP_Query();
	$get_posts = new WP_Query;
	$portfolio = $get_posts->query( $args );
//	$portfolio = get_posts($args);
//			$portfolio = get_posts($args);
			
	
	$posts_array = $portfolio;
//	$j =  PORTFOLIO_PER_PAGE * $_POST['page'];
	if ( is_array( $posts_array ) && ! empty( $posts_array ) ) {
		foreach ($posts_array as $item): setup_postdata($item);
			$post_terms = wp_get_post_terms( $item->ID, PORTFOLIO_TAX, $args );
			if ( !is_wp_error($post_terms) && is_array($post_terms) ) {
				$terms_classes ='';
				foreach ($post_terms as $key => $post_term) {
					$css_classes .= ' '. $post_term->slug;
				}
			}
   
   
			?>
            <div class="portfolio-block-two mix col-lg-6 col-md-6 col-sm-12 portfolio-<?php echo $item->ID ?> <?php echo $css_classes ?>" style="display: inline-block;">
                <div class="inner-box">
                    <?php if ( $image_url = get_the_post_thumbnail_url( $item, 'large' ) ): ?>
                    <div class="image">
                            <img class="article-img" src="<?php echo $image_url ?>" alt="<?php $image->the_post_thumbnail_alt( $item->ID );?>" loading="lazy">
                        <div class="overlay-box">
                            <a href="<?php echo $image_url ?>" data-fancybox="gallery-1" data-caption="" class="plus flaticon-plus"></a>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="lower-content">
                        <h5><a href="<?php the_permalink($item); ?>"><?php echo get_the_title($item) ?></a></h5>
						<?php echo get_the_term_list($item->ID, PORTFOLIO_TAX, '<div class="designation">', ',', '</div>'); ?>
                    </div>
                </div>
            </div>
		
			
			<?php
			wp_reset_postdata();
//			$j++;
		endforeach;
	}
	die(); // here we exit the script and even no wp_reset_query() required!
}
add_action('wp_ajax_lazyloadportfolio', 'lazyloadportfolio_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_lazyloadportfolio', 'lazyloadportfolio_ajax_handler'); // wp_ajax_nopriv_{action}