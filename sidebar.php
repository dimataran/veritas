<?php
if ( ( is_single() || is_page() ) && in_array( get_post_meta( get_queried_object_id(), '_et_pb_page_layout', true ), array( 'et_full_width_page', 'et_no_sidebar' ) ) ) {
	return;
}

if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
    <div class="sidebar-side col-lg-4 col-md-12 col-sm-12">
        <aside class="sidebar default-sidebar">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
        </aside>
	</div>
<?php
endif; ?>

