<?php

/**
 * Registers the `Service` post type.
 */
function post_service_init() {
	register_post_type(
		'service',
		[
			'labels'                => [
				'name'                  => __( 'Послуги', 'veritas' ),
				'singular_name'         => __( 'Service', 'veritas' ),
				'all_items'             => __( 'All Services', 'veritas' ),
				'archives'              => __( 'Service Archives', 'veritas' ),
				'attributes'            => __( 'Service Attributes', 'veritas' ),
				'insert_into_item'      => __( 'Insert into Service', 'veritas' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Service', 'veritas' ),
				'featured_image'        => _x( 'Featured Image', 'Service', 'veritas' ),
				'set_featured_image'    => _x( 'Set featured image', 'Service', 'veritas' ),
				'remove_featured_image' => _x( 'Remove featured image', 'Service', 'veritas' ),
				'use_featured_image'    => _x( 'Use as featured image', 'Service', 'veritas' ),
				'filter_items_list'     => __( 'Filter Service list', 'veritas' ),
				'items_list_navigation' => __( 'Service list navigation', 'veritas' ),
				'items_list'            => __( 'Service list', 'veritas' ),
				'new_item'              => __( 'New Service', 'veritas' ),
				'add_new'               => __( 'Add New', 'veritas' ),
				'add_new_item'          => __( 'Add New Service', 'veritas' ),
				'edit_item'             => __( 'Edit Service', 'veritas' ),
				'view_item'             => __( 'View Service', 'veritas' ),
				'view_items'            => __( 'View Service', 'veritas' ),
				'search_items'          => __( 'Search Service', 'veritas' ),
				'not_found'             => __( 'No Service found', 'veritas' ),
				'not_found_in_trash'    => __( 'No Service found in trash', 'veritas' ),
				'parent_item_colon'     => __( 'Parent Service:', 'veritas' ),
				'menu_name'             => __( 'Service', 'veritas' ),
			],
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => [ 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ],
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-admin-post',
			'show_in_rest'          => true,
			'rest_base'             => 'service',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		]
	);

}

add_action( 'init', 'post_service_init' );

/**
 * Sets the post updated messages for the `post_nasa_gallery` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `post_nasa_gallery` post type.
 */
function post_service_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['Service'] = [
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Service updated. <a target="_blank" href="%s">View Service</a>', 'veritas' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'veritas' ),
		3  => __( 'Custom field deleted.', 'veritas' ),
		4  => __( 'Service updated.', 'veritas' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Service restored to revision from %s', 'veritas' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Service published. <a href="%s">View Service</a>', 'veritas' ), esc_url( $permalink ) ),
		7  => __( 'Service saved.', 'veritas' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Service submitted. <a target="_blank" href="%s">Preview Service</a>', 'veritas' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Service scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Service</a>', 'veritas' ), date_i18n( __( 'M j, Y @ G:i', 'veritas' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Service draft updated. <a target="_blank" href="%s">Preview Service</a>', 'veritas' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	];

	return $messages;
}

add_filter( 'post_updated_messages', 'post_service_updated_messages' );

/**
 * Sets the bulk post updated messages for the `post_nasa_gallery` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `post_nasa_gallery` post type.
 */
function post_service_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['Service'] = [
		/* translators: %s: Number of Service. */
		'updated'   => _n( '%s Service updated.', '%s Service updated.', $bulk_counts['updated'], 'veritas' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Service not updated, somebody is editing it.', 'veritas' ) :
						/* translators: %s: Number of Service. */
						_n( '%s Service not updated, somebody is editing it.', '%s Service not updated, somebody is editing them.', $bulk_counts['locked'], 'veritas' ),
		/* translators: %s: Number of Service. */
		'deleted'   => _n( '%s Service permanently deleted.', '%s Service permanently deleted.', $bulk_counts['deleted'], 'veritas' ),
		/* translators: %s: Number of Service. */
		'trashed'   => _n( '%s Service moved to the Trash.', '%s Service moved to the Trash.', $bulk_counts['trashed'], 'veritas' ),
		/* translators: %s: Number of Service. */
		'untrashed' => _n( '%s Service restored from the Trash.', '%s Service restored from the Trash.', $bulk_counts['untrashed'], 'veritas' ),
	];

	return $bulk_messages;
}

add_filter( 'bulk_post_updated_messages', 'post_service_bulk_updated_messages', 10, 2 );
