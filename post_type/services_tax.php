<?php
    add_action( 'init', function (){
        $labels = array(
            'name' => 'Service Category',
            'singular_name' => 'Service Category',
            'menu_name' => 'Service Category',
            'all_items' => 'All Service Category',
            'edit_item' => 'Edit Service Category',
            'view_item' => 'View Service Category',
            'update_item' => 'Update Service Category',
            'add_new_item' => 'Add New Service Category',
            'new_item_name' => 'New Category Name',
            'parent_item' => 'Parent Service Category',
            'parent_item_colon' =>  'Parent Service Category',
            'search_items' => 'Search Service Category',
            'popular_items' => 'Popular Service Category',
            'separate_items_with_commas' => 'Separate Service Category with commas',
            'add_or_remove_items' => 'Add or remove Service Category',
            'choose_from_most_used' => 'Choose from the most used Service Category',
            'not_found' => 'No Service Category found.',
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
            'meta_box_cb' => null,
            'show_admin_column' => true,
            'hierarchical' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'service_cat', 'with_front' => true, 'hierarchical' => true, ),
            'sort' => true,
        );
        register_taxonomy( 'service_cat', array( 'service' ) , $args );
    });
	
	add_action( 'init', function (){
		$labels = array(
			'name' => 'Portfolio Category',
			'singular_name' => 'Portfolio Category',
			'menu_name' => 'Portfolio Category',
			'all_items' => 'All Portfolio Category',
			'edit_item' => 'Edit Portfolio Category',
			'view_item' => 'View Portfolio Category',
			'update_item' => 'Update Portfolio Category',
			'add_new_item' => 'Add New Portfolio Category',
			'new_item_name' => 'New Category Name',
			'parent_item' => 'Parent Portfolio Category',
			'parent_item_colon' =>  'Parent Portfolio Category',
			'search_items' => 'Search Portfolio Category',
			'popular_items' => 'Popular Portfolio Category',
			'separate_items_with_commas' => 'Separate Portfolio Category with commas',
			'add_or_remove_items' => 'Add or remove Portfolio Category',
			'choose_from_most_used' => 'Choose from the most used Portfolio Category',
			'not_found' => 'No Portfolio Category found.',
		);
		$args = array(
			'labels' => $labels,
			'public' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'meta_box_cb' => null,
			'show_admin_column' => true,
			'hierarchical' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'portfolio_cat', 'with_front' => true, 'hierarchical' => true, ),
			'sort' => true,
		);
		register_taxonomy( 'portfolio_cat', array( 'portfolio' ) , $args );
	});