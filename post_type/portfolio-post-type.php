<?php

/**
 * Registers the `Portfolio` post type.
 */
function post_portfolio_init() {
	register_post_type(
		'portfolio',
		[
			'labels'                => [
				'name'                  => __( 'Portfolio', 'veritas' ),
				'singular_name'         => __( 'Portfolio', 'veritas' ),
				'all_items'             => __( 'All Portfolios', 'veritas' ),
				'archives'              => __( 'Portfolio Archives', 'veritas' ),
				'attributes'            => __( 'Portfolio Attributes', 'veritas' ),
				'insert_into_item'      => __( 'Insert into Portfolio', 'veritas' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Portfolio', 'veritas' ),
				'featured_image'        => _x( 'Featured Image', 'Portfolio', 'veritas' ),
				'set_featured_image'    => _x( 'Set featured image', 'Portfolio', 'veritas' ),
				'remove_featured_image' => _x( 'Remove featured image', 'Portfolio', 'veritas' ),
				'use_featured_image'    => _x( 'Use as featured image', 'Portfolio', 'veritas' ),
				'filter_items_list'     => __( 'Filter Portfolio list', 'veritas' ),
				'items_list_navigation' => __( 'Portfolio list navigation', 'veritas' ),
				'items_list'            => __( 'Portfolio list', 'veritas' ),
				'new_item'              => __( 'New Portfolio', 'veritas' ),
				'add_new'               => __( 'Add New', 'veritas' ),
				'add_new_item'          => __( 'Add New Portfolio', 'veritas' ),
				'edit_item'             => __( 'Edit Portfolio', 'veritas' ),
				'view_item'             => __( 'View Portfolio', 'veritas' ),
				'view_items'            => __( 'View Portfolio', 'veritas' ),
				'search_items'          => __( 'Search Portfolio', 'veritas' ),
				'not_found'             => __( 'No Portfolio found', 'veritas' ),
				'not_found_in_trash'    => __( 'No Portfolio found in trash', 'veritas' ),
				'parent_item_colon'     => __( 'Parent Portfolio:', 'veritas' ),
				'menu_name'             => __( 'Portfolio', 'veritas' ),
			],
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
            'supports'              => [ 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ],
            'has_archive'           => true,
            'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-admin-post',
			'show_in_rest'          => true,
			'rest_base'             => 'portfolio',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		]
	);

}

add_action( 'init', 'post_portfolio_init' );

/**
 * Sets the post updated messages for the `post_nasa_gallery` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `post_nasa_gallery` post type.
 */
function post_portfolio_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['portfolio'] = [
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Portfolio updated. <a target="_blank" href="%s">View Portfolio</a>', 'veritas' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'veritas' ),
		3  => __( 'Custom field deleted.', 'veritas' ),
		4  => __( 'Portfolio updated.', 'veritas' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Portfolio restored to revision from %s', 'veritas' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Portfolio published. <a href="%s">View Portfolio</a>', 'veritas' ), esc_url( $permalink ) ),
		7  => __( 'Portfolio saved.', 'veritas' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Portfolio submitted. <a target="_blank" href="%s">Preview Portfolio</a>', 'veritas' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Portfolio scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Portfolio</a>', 'veritas' ), date_i18n( __( 'M j, Y @ G:i', 'veritas' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Portfolio draft updated. <a target="_blank" href="%s">Preview Portfolio</a>', 'veritas' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	];

	return $messages;
}

add_filter( 'post_updated_messages', 'post_portfolio_updated_messages' );

/**
 * Sets the bulk post updated messages for the `post_nasa_gallery` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `post_nasa_gallery` post type.
 */
function post_portfolio_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['portfolio'] = [
		/* translators: %s: Number of Portfolio. */
		'updated'   => _n( '%s Portfolio updated.', '%s Portfolio updated.', $bulk_counts['updated'], 'veritas' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Portfolio not updated, somebody is editing it.', 'veritas' ) :
						/* translators: %s: Number of Portfolio. */
						_n( '%s Portfolio not updated, somebody is editing it.', '%s Portfolio not updated, somebody is editing them.', $bulk_counts['locked'], 'veritas' ),
		/* translators: %s: Number of Portfolio. */
		'deleted'   => _n( '%s Portfolio permanently deleted.', '%s Portfolio permanently deleted.', $bulk_counts['deleted'], 'veritas' ),
		/* translators: %s: Number of Portfolio. */
		'trashed'   => _n( '%s Portfolio moved to the Trash.', '%s Portfolio moved to the Trash.', $bulk_counts['trashed'], 'veritas' ),
		/* translators: %s: Number of Portfolio. */
		'untrashed' => _n( '%s Portfolio restored from the Trash.', '%s Portfolio restored from the Trash.', $bulk_counts['untrashed'], 'veritas' ),
	];

	return $bulk_messages;
}

add_filter( 'bulk_post_updated_messages', 'post_portfolio_bulk_updated_messages', 10, 2 );
