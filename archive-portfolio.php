<?php get_header('law');
    global $custom_style;
	$postsCnt = wp_count_posts('portfolio');
	$post_count = $postsCnt->publish;
	
?>
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php $custom_style->theImgPath() ?>background/4.jpg)">
        <div class="container">
            <div class="content">
                <h1><?php post_type_archive_title(); ?></h1>
                <ul class="page-breadcrumb">
                    <li><a href="/"><?php _e('Головна', ''); ?></a></li>
                    <li><?php post_type_archive_title(); ?></li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Portfolio Section Two -->
    <section class="portfolio-section-two">
        <div class="container">
            <!-- Sec Title -->
            <div class="section-title centered">
                <div class="title">Практика</div>
                <h3>Наш практичний <span>досвід</span></h3>
            </div>

            <?php if ( have_posts() ) : ?>

            <!--MixitUp Galery-->
            <div class="mixitup-gallery">

                <!--Filter-->
                <div class="filters clearfix">
                    <?php
                        $args = [
                            'taxonomy' => PORTFOLIO_TAX,
                            'hide_empty' => true,
                        ];

/**
 *
 *     $terms = get_terms( array(
 *         'taxonomy' => 'post_tag',
 *         'hide_empty' => false,
 *     ) );
 *
 */


                    $terms = get_terms( $args);
                    
                    if (!empty($terms)):
                        ?>
                    <ul class="filter-tabs filter-btns text-center clearfix">
                        <li class="active filter" data-role="button" data-filter="all">All</li>
	                    <?php
		                   foreach ($terms as $key => $term):
	                    ?>
                        <li class="filter" data-role="button" data-filter=".<?php echo $term->slug ?>"><?php echo $term->name ?></li>
                      <?php
			                   endforeach;
			                   ?>
                    </ul>
                        <?php
                        endif;
                        ?>

                </div>


                <div id="portfolio_loop" class="filter-list row clearfix">


                 <?php   while ( have_posts() ) : the_post();
                     $url = get_the_post_thumbnail_url( null, 'Large' );

                     $alttext = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
	                 $terms = get_the_terms( get_the_ID(), PORTFOLIO_TAX );
	                 
	                 if ( is_wp_error( $terms ) ) {
		                 $terms = [];
		                 $css_classes = '';
	                 }
	                 
	                 if ( empty( $terms ) ) {
		                 $css_classes = '';
	                 }
	                 if(!is_array($terms)) {
                         $terms = [];
                     }
	                 foreach ( $terms as $term ) {
		                 $css_classes = ' ' . $term->slug;
	                 }
                  
                 ?>
                        <div class="portfolio-block-two mix col-lg-6 col-md-6 col-sm-12 portfolio-<?php echo get_the_ID() ?> <?php echo $css_classes ?>">
                            <div class="inner-box">
                                <div class="image">
                                    <img src="<?php echo $url ?>" alt="<?php echo $alttext ?>" />
                                    <div class="overlay-box">
                                        <a href="<?php echo $url ?>" data-fancybox="gallery-1" data-caption="" class="plus flaticon-plus"></a>
                                    </div>
                                </div>
                                <div class="lower-content">
                                    <h5><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h5>
                                    <?php
	                                     echo get_the_term_list(get_the_ID(), PORTFOLIO_TAX, '<div class="designation">', ',', '</div>');
                                    ?>
                                </div>
                            </div>
                        </div>
                     
                     
                    <?php
                        endwhile;
                        ?>
                 </div>
            </div>

        <?php endif; ?>
        <?php if ( $post_count >= PORTFOLIO_PER_PAGE ): ?>
            <!-- Button Box -->
<!--            <div class="button-box text-center">-->
                <button id="read_more" class="button-box text-center theme-btn btn-style-one btn-center">Більше</button>
<!--            </div>-->
        <?php endif; ?>

        </div>
    </section>

<?php //get_template_part('template-parts/subscription') ?>


<?php get_footer('law');