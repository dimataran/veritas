<?php get_header('law') ?>

    <!--Page Title-->
    <?php
    $url = get_the_post_thumbnail_url( null, 'Large' );
    if (empty($url)) {
    $url = 'https://www.advokat-veritas.kh.ua/wp-content/uploads/2021/01/vzyatka-300x175.jpg';
    }
    ?>
    <section class="page-title" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(<?php echo $url ?>)">
        <div class="container">
            <div class="content">
                <h1><?php the_title() ?></h1>
                <ul class="page-breadcrumb">
                    <li><a href="/"><?php _e('Головна', ''); ?></a></li>
                    <li><?php the_title() ?></li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
        <div class="container">
            <div class="row clearfix">

                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="blog-single">
                        <div class="inner-box">
                            <div class="image">
                                <img src="<?php echo $url ?>" alt="" />
                            </div>
                            <div class="lower-content">
                                <ul class="post-meta">
                                    <li><span class="fa fa-calendar"></span><?php echo get_the_date() ?></li>
                                    <li><span class="fa fa-user"></span><?php echo get_the_author() ?></li>
                                    <li><span class="fa fa-list"></span><?php echo get_the_category_list(', ') ?></li>
                                    <li><span class="fa fa-comment"></span><?php echo get_comments_number_text() ?></li>
                                </ul>

                                <div class="text">
                                    <?php echo apply_filters('the_content', get_the_content()) ?>
                                </div>
                            </div>
                        </div>

                        <!--post-share-options-->
                        <div class="post-share-options">
                            <div class="post-share-inner clearfix">
                                <div class="pull-left post-tags">
                                    <?php echo get_the_tag_list(' <span>Метки (тегі): </span>',', ',''); ?>

                                </div>
<!--                                <ul class="pull-right social-links clearfix">-->
<!--                                    <li class="facebook"><a href="#" class="fa fa-facebook"></a></li>-->
<!--                                    <li class="twitter"><a href="#" class="fa fa-twitter"></a></li>-->
<!--                                    <li class="google-plus"><a href="#" class="fa fa-google-plus"></a></li>-->
<!--                                    <li class="dribble"><a href="#" class="fa fa-dribbble"></a></li>-->
<!--                                </ul>-->
                            </div>
                        </div>

                        <!-- New Posts -->
                        <div class="new-posts">
                            <div class="clearfix">
                                <?php
                                previous_post_link('%link', '<span class="fa fa-angle-double-left"></span> %title');
                                next_post_link('%link', '%title <span class="fa fa-angle-double-right">');
                                ?>
                            </div>
                        </div>

                        <!--Comments Area-->
                        <?php
                        do_action( 'et_after_post' );

                        if ( ( comments_open() || get_comments_number() ) && 'on' === et_get_option( 'divi_show_postcomments', 'on' ) ) {
                            comments_template( '', true );
                        }
                        ?>



                    </div>
                </div>

                <?php get_sidebar(); ?>

            </div>
        </div>
    </div>
    <!--End Sidebar Page Container-->

    <!-- Subscribe Section -->

    <?php get_template_part('template-parts/subscription') ?>

    <!-- End Subscribe Section -->

    <?php get_footer('law') ?>
